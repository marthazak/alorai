# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 14:23:53 2020

@author: marthaZ
"""

import tkinter as tk


class Example(tk.Frame):
    def __init__(self, root):
        global frame
        global canvas
        global blubb
        tk.Frame.__init__(self, root)
        canvas = tk.Canvas(self, borderwidth=0, background="#ffffaa",width=500, height=600)

        frame = tk.Frame(canvas, background="#ffffff")
        vsb = tk.Scrollbar(root, orient="vertical", command=canvas.yview)
        canvas.configure(yscrollcommand=vsb.set)

        vsb.grid(row = 1, column = 1, sticky = "nsew")
        canvas.grid(row = 1, column = 0, sticky = "nsew")
        blubb = canvas.create_window(1,1, window=frame, anchor="nw", tags="frame")

        frame.bind("<Configure>", self.onFrameConfigure)
        canvas.bind('<Configure>', self.FrameSize)
        self.populate()
        
    def FrameSize(self, event):
        canvas_width = event.width
        canvas_height = event.height
        canvas.itemconfig(blubb, width = canvas_width, height = canvas_height)        

    def populate(self):

        button = tk.Button(root, text='Prev')
        button.grid(row=0, columns=1, sticky = "w")

        for row in range(1,100):
            tk.Label(frame, text="%s" % row, width=3, borderwidth="1", relief="solid").grid(row=row, column=0)
            t="this is the second column for row %s" %row
            tk.Label(frame, text=t).grid(row=row, column=1)

    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        canvas.configure(scrollregion=canvas.bbox("all"))

if __name__ == "__main__":
    root=tk.Tk()
    root.columnconfigure(0, weight = 1)
    root.rowconfigure(1, weight = 1)
    Example(root).grid(row = 1)
    root.mainloop()