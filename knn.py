import tkinter
from tkinter import * 
from tkinter.ttk import Progressbar
import webbrowser
from tempfile import NamedTemporaryFile
import progressbar as pb
from io import StringIO
from tkinter import filedialog
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from PIL import ImageTk, Image
from tkinter.simpledialog import askstring
from tkinter.messagebox import showinfo
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
import pandas as pd
import numpy as np
from tkinter import messagebox as TkMessageBox
import os
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn import metrics
from tkinter import ttk as ttk
#get_ipython().run_line_magic('matplotlib', 'inline')
from prettytable import PrettyTable
import biom
from biom import load_table
import cv2
import sys
import math
from tkintertable import TableCanvas, TableModel
import pickle
import CreateToolTip as CreateToolTip

if sys.version_info[0] < 3:
   import Tkinter as Tk
else:
   import tkinter as Tk
   

BG_COLOR ="#d9f6f6"
LARGE_FONT= ("Verdana", 12)
debug = 1   #testing
HEIGHT = 600
WIDTH = 800
advanced=1
maxNeighbor=100
class knn(Tk.Frame):
    
    
    def __init__(self, parent, controller,tmpdir):
        print("knn")
        global knnLabelFig
        global knnFeatureTable
        global knnErrVar   
        global exportButton
        global knnfeaturesNVar
        global canvas
        global frame_results
        self.tmpdir = tmpdir
        self.controller = controller
        self.parent = parent
        
        Tk.Frame.__init__(self,parent)


        canvas = Tk.Canvas(self,borderwidth=0, background=BG_COLOR, width=WIDTH, height=HEIGHT,scrollregion=(0,0,WIDTH+200,HEIGHT+500))
        #container.grid(row = 1, column = 0, sticky = "nsew")
        #container.pack(side="top", fill="both", expand=True)
        canvas.grid_rowconfigure(0, weight=1)
        canvas.grid_columnconfigure(0, weight=1)
        
        scrollbar = Tk.Scrollbar(self, orient="vertical")
        scrollbar.pack(fill = Y, side = RIGHT, expand = FALSE)
        scrollbar.config(command=canvas.yview)
        
        scrollbarx = Tk.Scrollbar(self, orient="horizontal")
        scrollbarx.pack(fill = X, side = BOTTOM, expand = FALSE)
        scrollbarx.config(command=canvas.xview)    
        canvas.configure(xscrollcommand = scrollbarx.set,yscrollcommand=scrollbar.set)
        canvas.config(width=WIDTH, height=HEIGHT)

#        canvas_frame = self.canvas.create_window((0,0), window=self, anchor="nw", tags="frame")
        canvas.pack(side=LEFT, expand=True, fill = BOTH)

        frame_results = Tk.Frame(canvas, bg=BG_COLOR)
        canvas_frame = canvas.create_window((0, 0), window=frame_results, anchor='nw', tags="frame")

        frame_results.bind("<Configure>", self.onFrameConfigure)
        canvas.bind('<Configure>', self.frameSize)


        label = Tk.Label(frame_results, text="K-nearest neighbor (KNN)", font=LARGE_FONT,bg=BG_COLOR)
        label.grid(row=0,pady=10,columnspan=6,padx=10)
                
        knnErrVar = StringVar("")        
        knnError = Tk.Label(frame_results,textvariable=knnErrVar,fg="red",bg=BG_COLOR)
        knnError.grid(row=1, column=0, columnspan=6, pady=2,padx=2)   

        if( advanced == 1):
            tuningFrame = Tk.LabelFrame(frame_results, text = "Tuning", padx=10, pady=10)
            tuningFrame.grid(row=2, columnspan = 6, sticky=W, padx=2)

             
            knnNeighborsDes = Tk.Label(tuningFrame, text="Enter the number of neighbors to use")
            knnNeighborsDes.grid(row=3, column=0,sticky=W,padx = 10,pady=1)
            
            global neighborVar
            neighborVar = StringVar(tuningFrame, value="5")   
            neighborEntry = Tk.Entry(tuningFrame, width=5,textvariable=neighborVar)
            neighborEntry.grid(row=3,column=1, sticky=W)
            tuningCTT = CreateToolTip(neighborEntry,"Must be a postitive number.")
   
    
            tuningWeightsDes = Tk.Label(tuningFrame, text="Select weights")
            tuningWeightsDes.grid(row=3, column=2,sticky=W,padx = 10,pady=1)

            global tuningWeightsVar
            WeightsVarChoices = {"uniform", "distance"}
            tuningWeightsVar = StringVar(tuningFrame, value="uniform")                       
            #tuningSolverVar.set('lbfgs') # set the default option
            tuningWeightsPopup = Tk.OptionMenu(tuningFrame, tuningWeightsVar, *WeightsVarChoices)
            tuningWeightsPopup.grid(row=3,column=3, sticky=W)
            tuningCTT = CreateToolTip(tuningWeightsPopup,"Weight function used in prediction. uniform: all points weighted equally. distance: weight points by the inverse of their distance.")

            featureButton = Tk.Button(tuningFrame, text = 'Show', command=lambda: self.show_frame(what=2), bg='blue', fg='white')
            featureButton.grid(row=3, column=4,sticky=E, padx=50)  
            rowN = 4
        else:
            rowN = 2
                   
        featuresFrame2 = Tk.LabelFrame(frame_results, text = "Evaluation", padx=10, pady=10)
        featuresFrame2.grid(row=rowN, columnspan = 6, sticky=W, padx=2)               
 
    
        
        rocimage = Tk.PhotoImage(file="images/placeholder.png")
        global knnroclabel
        figureROC = ImageTk.PhotoImage(Image.open("images/placeholder.png"))
        knnroclabel = Tk.Label(featuresFrame2)
        knnroclabel.image = rocimage # keep a reference!
        knnroclabel.grid(row=rowN+1, column=0,columnspan=2,pady=10,padx=10)
             
        global knnprlabel
        knnprlabel = Tk.Label(featuresFrame2)
        knnprlabel.image = rocimage # keep a reference!
        knnprlabel.grid(row=rowN+1,column=2,columnspan=2,pady=10,padx=10)
        
        global knncmlabel
        knncmlabel = Tk.Label(featuresFrame2)
        knncmlabel.image = rocimage # keep a reference!
        knncmlabel.grid(row=rowN+2,column=0,columnspan=2,pady=10,padx=10)
        
        global knnperformancelabel
        knnperformancelabel = Tk.Label(featuresFrame2)
        knnperformancelabel.image = rocimage
        knnperformancelabel.grid(row=rowN+2,column=2,columnspan=2,pady=10,padx=10)    

        rowN = rowN + 3
 
#######################################
# knn does not support feature selection, so comment this
#         featuresFrame = Tk.LabelFrame(frame_results, text = "Relevant Features", padx=10, pady=10)
#         featuresFrame.grid(row=rowN, columnspan = 6, sticky=W, padx=2)

#         featuresFrameQ = Tk.LabelFrame(featuresFrame, text = "", padx=10, pady=10, bg=BG_COLOR)
#         featuresFrameQ.grid(row=rowN+1, columnspan = 6, sticky=W, padx=2) 
        
#         featuresDes = Tk.Label(featuresFrameQ, text="Select number of features (taxa) most important in model", bg=BG_COLOR)
#         featuresDes.grid(row=rowN+2, column=0,sticky=W,padx = 10,pady=1)
        
#         knnfeaturesNVar = StringVar(featuresFrameQ, value="10")   
#         featureEntry = Tk.Entry(featuresFrameQ, width=5,textvariable=knnfeaturesNVar)
#         featureEntry.grid(row=rowN+2,column=1, sticky=W)
# #        featureTT = CreateToolTip(featureEntry,"Number of most contributing features")
   

#         featureButton = Tk.Button(featuresFrameQ, text = 'Show', command=lambda: controller.show_frame(logisticRegression), bg='blue', fg='white')
#         featureButton.grid(row=rowN+2, column=2,sticky=E, padx=50)         
        
#         resultFrame = Frame(featuresFrame)  
#         resultFrame.grid(row=rowN+3,columnspan=3,pady=30)  
#         data = {'rec1': {'Feature': 'test', 'Score': 108.79, 'Class': 0},'rect2': {'Feature':'test2','Score': 108.79, 'Class': 1}} 

#         knnFeatureTable = TableCanvas(resultFrame, data = data,
#                                editable=False,read_only=True,
#                                reverseorder=1)
#         knnFeatureTable.show()  

        # Create a Tkinter variable
#        global knnexportTableVar
#        knnexportTableVar = StringVar(featuresFrame)

        # # Dictionary with options
        # choices = {"comma seperated - CSV", 
        #            "tab seperated - TSV",
        #            "excel file - XLSX" }
        # knnexportTableVar.set('comma seperated - CSV') # set the default option

        # exportPopupMenu = Tk.OptionMenu(featuresFrame, knnexportTableVar, *choices)
        # exportPopupMenu.grid(row = rowN+4, column = 0,padx = 2, pady=1,sticky=E)

        # exportButton = Tk.Button(featuresFrame, text = 'Export table', command=browse_export_feature, fg = 'white', bg = 'purple')
        # exportButton.grid(row=rowN+4,column=1,sticky=W)
    def onFrameConfigure(self, event):
        canvas.configure(scrolknnegion=canvas.bbox("all"))

    def frameSize(self, event):
        canvas_width = event.width
        canvas_height = event.height
        canvas.itemconfig(frame_results, width = canvas_width, height = canvas_height)         
    
    def show_frame(self, what):
        knnErrVar.set("")  
        if(what == 2):  #what is to save the model, 2: y, 1 no
            try:
                neighbor = int(neighborVar.get())               
            except ValueError:
                knnErrVar.set("Should be a number between 1 and 100.")      
            else:
                if(int(neighborVar.get()) >=1 and int(neighborVar.get())<=maxNeighbor):
                    self.controller.calculateModel('KNN', KNeighborsClassifier(n_neighbors=int(neighborVar.get()),weights=tuningWeightsVar.get()))
                    knnErrVar.set("")    
                else:
                    knnErrVar.set("Number of neighbors between 1 and 100.")

    
        filename = os.path.join(self.tmpdir,'KNN_ml.sav')
        global knnroclabel
        figureROC = ImageTk.PhotoImage(Image.open( os.path.join(self.tmpdir,'KNN_roccurve.jpg')))
        knnroclabel.configure(image=figureROC)
        knnroclabel.image = figureROC
        global knnprlabel
        knnfigurePR = ImageTk.PhotoImage(Image.open( os.path.join(self.tmpdir,'KNN_prcurve.jpg')))
        knnprlabel.configure(image=knnfigurePR)
        knnprlabel.image = knnfigurePR
        global knnperformancelabel
        knnfigurePerformance = ImageTk.PhotoImage(Image.open( os.path.join(self.tmpdir,'KNN_performance.jpg')))
        knnperformancelabel.configure(image=knnfigurePerformance)
        knnperformancelabel.image = knnfigurePerformance
        global knncmlabel
        knnfigureCM = ImageTk.PhotoImage(Image.open( os.path.join(self.tmpdir,'KNN_confusionmatrix.jpg')))
        knncmlabel.configure(image=knnfigureCM)
        knncmlabel.image = knnfigureCM    
                         
def browse_export_feature():
    global featureScoresN
    global exportTableVar
    
    if(exportTableVar.get() == 'comma seperated - CSV'):
        savefile = filedialog.asksaveasfilename(filetypes=(("Comma seperated file", "*.csv"),
                                                     ("All files", "*.*") ))               
        featureScoresN.to_csv(savefile + ".csv", index=False)    
        mostAbundantErrVar.set("Saving completed")
    elif(exportTableVar.get() == "excel file - XLSX"):
        savefile = filedialog.asksaveasfilename(filetypes=(("Excel file", "*.csv"),
                                                     ("All files", "*.*") ))               
        featureScoresN.to_excel(savefile + ".xlsx", index=False, sheet_name='Results')                  
        mostAbundantErrVar.set("Saving completed")
    elif(exportTableVar.get() == 'tab seperated - TSV'):
        savefile = filedialog.asksaveasfilename(filetypes=(("Tab seperated file", "*.tab"),
                                                     ("All files", "*.*") ))               
        featureScoresN.to_csv(savefile + ".tab", index=False,sep="\t")
        mostAbundantErrVar.set("Saving completed")
    else:
        mostAbundantErrVar.set("Failed to save file")
#        except:
#            smostAbundantErrVar.set("Failed to save file")
  
def getBestFeature(numberToShow, loaded_model,table,diversityDF):
    import numpy as np
    import pandas as pd
    global tmpdir
    
    tableDF = table.to_dataframe(dense=1)

    #todo generic, such that other will be implemented too
    features = np.concatenate((tableDF.index.values, diversityDF.columns.values)).tolist()

    importance = loaded_model.coef_[0]
    classpred = []
    for x in importance:
        if(x > 0):
            classpred.append("class 1")
        else:
            classpred.append("class 0")

    featureScores = {'Feature':features,
                     'Score':np.absolute(importance),
                     'Class':classpred}

    df = pd.DataFrame(featureScores)

    #print sorted table of n = 20
    featureScoresN = df.nlargest(numberToShow,'Score')    
    return(featureScoresN.to_dict('index'))

class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = Tk.Label(self.tw, text=self.text, justify='left',
                       background='white', relief='solid', borderwidth=1,
                       font=("times", "8", "normal"))
        label.pack(ipadx=1)
    def close(self, event=None):
        if self.tw:
            self.tw.destroy()