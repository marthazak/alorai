#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
from sklearn.decomposition import PCA 
from sklearn.preprocessing import StandardScaler


# In[2]:


data = pd.read_excel(r'C:\Work\repository\Alora\example\ILEUM - MET.S.xlsx')
data = data.dropna()


# In[3]:


data.columns.get_loc("MET.S.TG")


# In[4]:


X = data.iloc[:,1:400].values 
y = data.iloc[:,472].values


# In[5]:


# split X and y into training and testing sets
from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.25,random_state=0)


# In[6]:


# import the class
from sklearn.linear_model import LogisticRegression

# instantiate the model (using the default parameters)
logreg = LogisticRegression()

# fit the model with data
logreg.fit(X_train,y_train)
importance = logreg.coef_[0]
features = list(data.iloc[:,1:400].columns)
classpred = []
for x in importance:
    if(x > 0):
        classpred.append("class 1")
    else:
        classpred.append("class 0")

featureScores = {'Feature':features,
                 'Score':np.absolute(importance),
                 'Class':classpred}

df = pd.DataFrame(featureScores)

#print sorted table of n = 20
print(df.nlargest(20,'Score'))

#print specific value if user wants to search
print(df.loc[df['Feature'].str.contains('Lachnospira')])


#plt.bar([x for x in range(len(importance))], importance)

#plt.show()
#
#y_pred=logreg.predict(X_test)



# import the metrics class
from sklearn import metrics
y_pred=logreg.predict(X_test)
cnf_matrix = metrics.confusion_matrix(y_test, y_pred)


# import required modules
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


# class_names=[0,1] # name  of classes
# fig, ax = plt.subplots()
# tick_marks = np.arange(len(class_names))
# plt.xticks(tick_marks, class_names)
# plt.yticks(tick_marks, class_names)
# # create heatmap
# sns.heatmap(pd.DataFrame(cnf_matrix), annot=True, cmap="YlGnBu" ,fmt='g')
# ax.xaxis.set_label_position("top")
# plt.tight_layout()
# plt.title('Confusion matrix', y=1.1)
# plt.ylabel('Actual label')
# plt.xlabel('Predicted label')
# plt.show()

print("Accuracy:",metrics.accuracy_score(y_test, y_pred))
print("Precision:",metrics.precision_score(y_test, y_pred))
print("Recall:",metrics.recall_score(y_test, y_pred))
from sklearn.metrics import classification_report
report = classification_report(y_test, y_pred)
print(report)

y_pred_proba = logreg.predict_proba(X_test)[::,1]
fpr, tpr, _ = metrics.roc_curve(y_test,  y_pred_proba)
auc = metrics.roc_auc_score(y_test, y_pred_proba)
plt.plot(fpr,tpr,label="auc="+str(auc))
plt.legend(loc=4)
plt.show()

import tempfile
import os
import pickle
tmpdir = tempfile.mkdtemp(dir = tempfile.gettempdir(),suffix="alora")
filename = os.path.join(tmpdir,'regresseion_ml.sav')
    
print(filename)
pickle.dump(logreg, open(filename, 'wb'))