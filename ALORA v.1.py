#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import tkinter
from tkinter import * 
from tkinter.ttk import Progressbar
import webbrowser
from tempfile import NamedTemporaryFile
import progressbar as pb
from io import StringIO
from tkinter import filedialog
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from PIL import ImageTk, Image
from tkinter.simpledialog import askstring
from tkinter.messagebox import showinfo
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
import pandas as pd
import numpy as np
from tkinter import messagebox as TkMessageBox
import os
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn import metrics
from tkinter import ttk as ttk
get_ipython().run_line_magic('matplotlib', 'inline')
from prettytable import PrettyTable
import biom
from biom import load_table
import cv2
import sys
import math
from tkintertable import TableCanvas, TableModel
import pickle
from knn import knn
from cart import cart


if sys.version_info[0] < 3:
   import Tkinter as Tk
else:
   import tkinter as Tk

global fname
global data
global name
global Location
global formatSelection
global mdata
global Lb1
mdata = pd.DataFrame() 
global labelInfo
global labelInfo2
global labelInfo3Text
global homeVarText
global tableRel
#relative abundance
global table
global fx,fy,dpi
global advanced
advanced = 1
fx=3
fy=2
dpi=70

#absolute, for diversity
global tableAbs

global debug
BG_COLOR ="#d9f6f6"
LARGE_FONT= ("Verdana", 12)
debug = 1   #testing
HEIGHT = 600
WIDTH = 800

def examplefile():
    print("reading annotation")
    upload_afile()
    global Location
    Location = 7
    print("reading countfile")
    upload_cfile()
    print('transfering data')
 
    transferData()
    print('calculate models')

def examplefile2():
    print("reading annotation")
    upload_afile()
    global Location
    Location = 7
    print("reading countfile")
    upload_cfile()
    print('transfering data')
 
    transferData()
    print('skipping taxonomy based files. just load model')
    global tmpdir
#    tmpdir = "C:/Users/marthaZ/AppData/Local/Temp/tmpoh5yhsexalora"
    
def addDiversity():
    return 1

#browse the annotation file
def browse_afile():
    global metadata_file
    metadata_file = filedialog.askopenfilename(filetypes = (("All files", "*"), ("Template files", "*.type"), ("Biom file", "*biom"), ("Tab file", "*tsv")))
    name1, ext = os.path.splitext(metadata_file)   
    if(not(ext.lower() == ".csv" or ext.lower() == ".tsv" or ext.lower() == ".xls" or ext.lower() == ".xlsx")):
        showinfo("Message","Wrong file format! Please upload a csv, tsv or excel file")
    
#browse the counts file
def browse_cfile():
    global countsdata_file
    countsdata_file = filedialog.askopenfilename(filetypes = (("All files", "*"), ("Template files", "*.type"), ("Biom file", "*biom"), ("Tab file", "*tsv")))
    name1, ext = os.path.splitext(countsdata_file)   
    if(not(ext.lower() == ".biom" or ext.lower() == ".tsv")):
        showinfo("Message","Wrong file format for file with extension: "+ext.lower()+" Please upload a biom or tsv file")
    
def browse_export_feature():
    global featureScoresN
    global exportTableVar
    
    if(exportTableVar.get() == 'comma seperated - CSV'):
        savefile = filedialog.asksaveasfilename(filetypes=(("Comma seperated file", "*.csv"),
                                                     ("All files", "*.*") ))               
        featureScoresN.to_csv(savefile + ".csv", index=False)    
        mostAbundantErrVar.set("Saving completed")
    elif(exportTableVar.get() == "excel file - XLSX"):
        savefile = filedialog.asksaveasfilename(filetypes=(("Excel file", "*.csv"),
                                                     ("All files", "*.*") ))               
        featureScoresN.to_excel(savefile + ".xlsx", index=False, sheet_name='Results')                  
        mostAbundantErrVar.set("Saving completed")
    elif(exportTableVar.get() == 'tab seperated - TSV'):
        savefile = filedialog.asksaveasfilename(filetypes=(("Tab seperated file", "*.tab"),
                                                     ("All files", "*.*") ))               
        featureScoresN.to_csv(savefile + ".tab", index=False,sep="\t")
        mostAbundantErrVar.set("Saving completed")
    else:
        mostAbundantErrVar.set("Failed to save file")
#        except:
#            smostAbundantErrVar.set("Failed to save file")

#upload the counts file
def upload_cfile():
    from biom.table import Table
    global countsdata_file
    global cdata  
    global mdata  
    global table
    cformat = formatSelection2.get()    
#exampmle
#    countsdata_file = "L:/Joint_Projects/Helminth_Genome/graham/megarun/genotype/calypso/counts_testing.biom"
    if(debug):
       # countsdata_file = "L:/Joint_Projects/Helminth_Genome/graham/megarun/genotype/calypso/counts_testing.biom"
       countsdata_file = "L:/Joint_Projects/Helminth_Genome/graham/megarun/genotype/calypso/counts_stool.biom"
    if(cformat == "biom"):
        table = biom.load_table(countsdata_file)  #counts
        # get sample ids  #sample_ids
        sample_ids = table.ids() #sample_ids
        # get observation ids  #observ_ids
        obs_ids = table.ids(axis='observation')  #otu ids    
        # get observation metadata
        obs_meta = table.metadata(axis='observation')   #taxonomy
        # Using a for loop to iterate through a tuple to rename the otus
        otus = []
        runner = 0
        npotus = np.array([])
        for element in obs_meta:
            taxonomy = element['taxonomy']
            highest = "Undefine"
            lowest = ""
            for i in taxonomy:
                if(i == "Unassigned"):
                    highest = i
                elif(i.startswith('p__')):
                    highest = i
                elif(i.startswith('s__')):
                     if(len(i) > 3):
                         lowest = lowest + "_" + i[3:]
                elif(i.startswith('k__')):
                    highest = i
                elif(i.startswith('g__') and len(i) > 3):
                    if(len(i) > 3): 
                        lowest = i
                elif(len(i) > 3):
                    lowest = i
            if(lowest == ""):
                npotus = np.append(npotus,obs_ids[runner])  
                otus.append(obs_ids[runner].replace("New.CleanUp.ReferenceOTU","nOTU")+"_"+highest)
            else:
                npotus = np.append(npotus, obs_ids[runner])  
                otus.append(obs_ids[runner].replace("New.CleanUp.ReferenceOTU","nOTU")+"_"+highest+"_"+lowest)             
            runner = runner + 1 
        tabledata = table.to_dataframe(dense=1).to_numpy()
        #replace table by new taxonomic colunn for observation id
        table = Table(tabledata,  np.array(otus), sample_ids, None, None, table_id='renamed_table')
        
        #check if sammple ids are in annotation fiel
        sample_metafile = list(mdata.index.values)
        overlap = np.in1d(sample_ids,mdata.index.values) #todo check which are missing
        if(all(overlap) == 1):
            labelInfo3Text.set("Successfully uploaded counts file with X rows and y columns.")          
        else:
            missingBiomSamples = sample_ids[np.invert(overlap)]
            labelInfo3Text.set("Excluded missing samples "+np.array2string(missingBiomSamples, separator=', '))            
            sample_ids = sample_ids[overlap]
            table.filter(sample_ids, axis='sample', invert=False)
    elif(cformat == "tsv"):
        #todo
        print(cformat)
 #   if(len(count.axes[0]) < 2 or len(mdata.axes[1]) < 2):
 #        showinfo("Message","Error while importing metadata file. Data is missing in metadata file")       
 #   else:
 #       labelInfo.config(text="Successfully uploaded metadata file")
 
def filterMin():
    global table
    global tableRel
    global minSampleDefault
    global rareTaxaDefault
    
    sampleIds=table.ids()
    otus = table.ids(axis='observation')

    #exclude taxa with low abundance (sequencing error e.g.)
    tableRel = table.norm(inplace=False)
    taxMean = tableRel.sum(axis='observation') / tableRel.length(axis='sample') * 100

    taxMeanFilter = taxMean < float(rareTaxaDefault.get())
    excludeTax = sum(taxMeanFilter)
    failedTax = otus[taxMeanFilter]
    table.filter(failedTax, axis='observation', invert=True) 
    
    #exclude samples with low sample size
    aSum = table.sum(axis='sample') 
    aSumFilter = aSum < float(minSampleDefault.get())
    excludeSample = sum(aSumFilter)
    failedSample = sampleIds[aSumFilter]
    table.filter(failedSample, axis='sample', invert=True) 
    
    #apply relative abundance again of filtered table
    tableRel = table.norm(inplace=False)  
    
    return(str(excludeSample)+" samples and "+str(excludeTax)+" features removed.")
       
#upload annotation file
def upload_afile():
    global metadata_file
    global mdata
    global labelInfo
    mformat = formatSelection.get()
    #example
 #   metadata_file = "L:/Joint_Projects/Helminth_Genome/graham/megarun/genotype/calypso/annotation_testing.csv"  #testing 
    if(debug):
        metadata_file = "L:/Joint_Projects/Helminth_Genome/graham/megarun/genotype/calypso/annotation_with_cfb.csv"  #testing 
    if(mformat == "comma seperated - CSV"):
            mdata = pd.read_csv(metadata_file,index_col=0) 
    elif(mformat == "excel file - XLSX"): #todo fix this, excel import does not work
            mdata = pd.read_excel(metadata_file,index_col=0)
    elif(mformat == "tab seperated - TSV"):
            mdata = pd.read_csv(metadata_file, sep='\t',index_col=0)
    mdata = mdata.dropna()
    if(len(mdata.axes[0]) < 2 or len(mdata.axes[1]) < 2):
        showinfo("Message","Error while importing metadata file. Data is missing in metadata file")       
    else:
        labelInfo.config(text="Successfully uploaded metadata file")
    update_step2list()
    
def select_target():
    global a
    global b
    global name
    global Location
    global X
    global Y
    global mdata
    mylist = mdata.columns.to_list()
    #name = askstring('BioStatistical Analysis', 'Please select the criteria to be analysed?')
    #showinfo('Selected Criteria', 'Selected Criteria is : {}'.format(name))
   
    def on_click_listbox(name):
        global Location
        global mdata
        widget = name.widget
        #get selected line index
        index = widget.curselection()
        #get the line's text
        name = widget.get(index[0])
        data1 = mdata[mdata[name] == 1]
        data2 = mdata[mdata[name] == 0]
        a = data1.shape[0]
        b = data2.shape[0]
        minimum = min(a,b)
        mdata = mdata.sample(n=len(mdata), random_state=42)
        Location = mdata.columns.get_loc(name)
        labelInfo.config(text=name)
        return(Location)
    
    
    app= Tk.Tk()
    app.title('Target variable')
    app.geometry('700x400')
    Label(app, text="Please select the target variable. This will be used as an outcome in the analysis.").pack()
    frame = Frame(app)
    frame.pack()    
    scrollbar = Scrollbar(app, orient="vertical")
    scrollbar.pack(side="left", fill="y")
    Lb1 = Listbox(frame, width = 450)
    Lb1.pack(side="left", fill="y")
    Lb1.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=Lb1.yview)

    

        

#Function responsible for the updation 
# of the progress bar value 
    def bar(): 
        import time 
        
        progress['value'] = 60
        root.update_idletasks() 
        time.sleep(0.5) 
  
        progress['value'] = 70
        root.update_idletasks() 
        time.sleep(0.5) 
  
        progress['value'] = 90
        root.update_idletasks() 
        time.sleep(0.5) 
  
 
    

    
    for met in mylist:
        Lb1.insert('end', met)
    Location = Lb1.bind('<<ListboxSelect>>', on_click_listbox)
    
    def quitapp():
        print("bin drin")
        #app.quit()  #todo close window, lock main windos: also refocus on this during opening so nothing can be changed in the main        
    button = Tk.Button(app, 
                          text = 'Select', 
                          command=quitapp, bg = 'blue', fg = 'white')
    button.pack(side="bottom",fill="y")
   
    app.mainloop()
 


def calculateModel(name, model):
        print("calculate model: "+name)
        global tableDF
        global Location
        global table
        tableDF = table.to_dataframe(dense=1).transpose()        
        tableDF = table.to_dataframe(dense=1)
        X = pd.concat([diversityDF, tableDF.transpose()], axis=1)    

        tabledfrownames = X.index.values
        overlapIdsorted = [s for s in tabledfrownames if s in mdata.index.values]    
        mdata_new = mdata.loc[overlapIdsorted]
        Y = mdata_new.iloc[:,Location].values
        Yunique = np.unique(Y)
        decode = Yunique
        if(len(Yunique) == 2 and not((Yunique[0]==0 and Yunique[1]==1) or (Yunique[0]==1 and Yunique[1]==0) )):
            Y[ Y==Yunique[0]] = 0
            Y[ Y==Yunique[1]] = 1
            Y=Y.astype('int')
            Yunique = [0,1] 
        
        seed = 7
        global Highest_accuracy 
        results = []
        names = []
        global means
        means = []
        cnf_matrices = []
        global TNs
        TNs = []
        global TPs
        TPs = []
        global FPs
        FPs = []
        global FNs
        FNs = []
        global Precisions
        Precisions = []
        global Recalls
        Recalls = []
        global Sensitivities
        Sensitivities = []
        global Specificities
        Specificities = []
        global accuracies 
        accuracies = []
        global images
        images = []
        global itg
        itg = []
        global dor
        dor = []
        scoring = 'accuracy'

        global tmpdir
        

        from sklearn.model_selection import train_test_split
        X_train,X_test,y_train,y_test=train_test_split(X,Y,test_size=0.3,random_state=0)
        m = model

        #fit the model with data
        m.fit(X_train,y_train)
        
        # save the model to disk
        filename = os.path.join(tmpdir,name+'_ml.sav')
        pickle.dump(m, open(filename, 'wb'))
        y_pred=m.predict(X_test)
        print("saving model to disk")
         
        #save confusion matrix for each model
        picturename = os.path.join(tmpdir,name+'_confusionmatrix.jpg')

        cm = confusion_matrix(y_test, y_pred)
        save_cm(cm, picturename, name, "", decode)
        print("saving confusion matrix")
        #save roc curve for each model
        from sklearn.metrics import roc_curve
        from sklearn.metrics import roc_auc_score  
        picturename = os.path.join(tmpdir,name+'_roccurve.jpg')
        ml_probs = m.predict_proba(X_test)
        ml_probs = ml_probs[:, 1]
        ml_auc = round(roc_auc_score(y_test, ml_probs),3)
        ml_fpr, ml_tpr, _ = roc_curve(y_test, ml_probs)
        save_roc(ml_fpr, ml_tpr, ml_auc, name, picturename)
            

    
        #save precision curve for each model
        from sklearn.metrics import precision_recall_curve
        from sklearn.metrics import f1_score
        from sklearn.metrics import auc
        picturename = os.path.join(tmpdir,name+'_prcurve.jpg')

        yhat = m.predict(X_test)
        ml_precision, ml_recall, _ = precision_recall_curve(y_test, ml_probs)
        ml_f1, ml_auc = f1_score(y_test, yhat), auc(ml_recall, ml_precision)
        save_pr(ml_recall, ml_precision, name, picturename, name+': f1=%.3f auc=%.3f' % (ml_f1, ml_auc))
       
        #create summary table for each model
        kfold = model_selection.KFold(n_splits=10, random_state=seed)
        cv_results = model_selection.cross_val_score(m, X, Y, cv=kfold, scoring=scoring)
        results.append(cv_results)   
        cm = confusion_matrix(y_test, y_pred) 
        TN = cm[0][0]
        TNs.append(TN)
        FN = cm[1][0]
        FNs.append(FN)
        TP = cm[1][1]
        TPs.append(TP)
        FP = cm[0][1]
        FPs.append(FP)
        if(FN == 0 or TN == 0):
             DOR = 0
        else:
             DOR = round((TP/FN)/(FP/TN),2)
        Precision = round(metrics.precision_score(y_test, y_pred, pos_label=Yunique[0]),2)
        Precisions.append(round(Precision*100,2))
        Recall = round(metrics.recall_score(y_test, y_pred, pos_label=Yunique[0]),2)
        Sensitivity = round(TP/(TP+FN),2)
        Sensitivities.append(round(Sensitivity*100,2))
        Specificity = round(TN/(TN+FP),2)
        Specificities.append(round(Specificity*100,2))
        acc = "%.2f (%.2f)" % (round((cv_results.mean()*100),2), round((cv_results.std()*100),2))
        summaryTable = pd.DataFrame() 
        summaryTable[name]=['Accuracy','Recall','Precision','Sensitivity','Specificity','DOR']
        summaryTable['Value'] = [acc,Recall,Precision,Sensitivity,Specificity,DOR]

        render_mpl_table(summaryTable,os.path.join(tmpdir,name+'_performance.jpg'),header_columns=0, col_width=2.0)

 
##################################
#do calculate all models and save figures, for each model and the summary frame
#what
#   1: save model
def calculateModels(app,what=1):
    global Location
    global table


   
    # Progress bar widget 
    s = ttk.Style()
    s.theme_use('clam')
    s.configure("colour.Horizontal.TProgressbar", foreground='red', background='black')
    popup = Tk.Toplevel()
    popup.grab_set()
    Tk.Label(popup, text="Calculating models").grid(row=0,column=0)

    progress = ttk.Progressbar(popup, orient = HORIZONTAL, style="colour.Horizontal.TProgressbar",
            length = 300, mode = 'indeterminate') 
    progress.grid(row=1, column=0)
    app.pack_slaves()
    


# #Function responsible for the updation 
# # of the progress bar value 
    def barN(): 
        import time 
        pvalue = []
        
        for i in range(1,5):
            pvalue.append(100/(4*modelsN)*i+100/modelsN*(step-1))
        progress['value'] = pvalue[0]
        popup.update() 
        time.sleep(0.5)  
        progress['value'] = pvalue[1]
        popup.update_idletasks() 
        time.sleep(0.5)  
        progress['value'] = pvalue[2]
        popup.update() 
        time.sleep(0.5) 
        progress['value'] = pvalue[3]
        popup.update() 
        time.sleep(0.5)  
   
    tableDF = table.to_dataframe(dense=1)
    X = pd.concat([diversityDF, tableDF.transpose()], axis=1)    
    features = X.columns

    
    tabledfrownames = X.index.values
    overlapIdsorted = [s for s in tabledfrownames if s in mdata.index.values]    
    mdata_new = mdata.loc[overlapIdsorted]
    Y = mdata_new.iloc[:,Location].values
    Yunique = np.unique(Y)
    decode = Yunique
    if(len(Yunique) == 2 and not((Yunique[0]==0 and Yunique[1]==1) or (Yunique[0]==1 and Yunique[1]==0) )):
        Y[ Y==Yunique[0]] = 0
        Y[ Y==Yunique[1]] = 1
        Y=Y.astype('int')
        Yunique = [0,1]

    
        
    seed = 7
#     # prepare models
    models = []
    models.append(('LR', LogisticRegression()))
#    models.append(('LDA', LinearDiscriminantAnalysis()))
#    models.append(('KNN', KNeighborsClassifier()))
#    models.append(('KNN - 25', KNeighborsClassifier(n_neighbors=25)))
    models.append(('CART', DecisionTreeClassifier()))
#    models.append(('NB', GaussianNB()))
#    models.append(('SVM - Poly', SVC(kernel='poly', degree = 8)))
#    models.append(('SVM - Linear', SVC(kernel='linear')))
#    models.append(('SVM - Sigmoid', SVC(kernel='sigmoid')))
#    models.append(('SVM - RBF', SVC(kernel='rbf')))
    # evaluate each model in turn

    global Highest_accuracy 
    results = []
    names = []
    global means
    means = []
    cnf_matrices = []
    global TNs
    TNs = []
    global TPs
    TPs = []
    global FPs
    FPs = []
    global FNs
    FNs = []
    global Precisions
    Precisions = []
    global Recalls
    Recalls = []
    global Sensitivities
    Sensitivities = []
    global Specificities
    Specificities = []
    global accuracies 
    accuracies = []
    global images
    images = []
    global itg
    itg = []
    global dor
    dor = []
    
    mls_fpr = []
    mls_tpr = []
    mls_auc = []
    mls_recall = []
    mls_precision  = []
    mls_f1 = []
    scoring = 'accuracy'
    modelsN = len(models)
    
 
    step = 1  #for progressbar

    figAllR = plt.figure(num=3,dpi=dpi)
    figAllP = plt.figure(num=5,dpi=dpi)
    from sklearn.model_selection import train_test_split
    X_train,X_test,y_train,y_test=train_test_split(X,Y,test_size=0.3,random_state=0)
    

    for name, model in models:

        barN()
        m = model
        print("calculating model "+name)
        #fit the model with data
        m.fit(X_train,y_train)
        # save the model to disk
        filename = os.path.join(tmpdir,name+'_ml.sav')
        pickle.dump(m, open(filename, 'wb'))
        y_pred=m.predict(X_test)
 
      
        #calculate roc curve for each model
        from sklearn.metrics import roc_curve
        from sklearn.metrics import roc_auc_score  
        ml_probs = m.predict_proba(X_test)
        ml_probs = ml_probs[:, 1]
        ml_auc = round(roc_auc_score(y_test, ml_probs),3)
        ml_fpr, ml_tpr, _ = roc_curve(y_test, ml_probs)
        mls_fpr.append(ml_fpr)
        mls_tpr.append(ml_tpr)
        mls_auc.append(ml_auc)

        #add each roc curve for comparison image including all models
        plt.figure(3)
        plt.plot(ml_fpr, ml_tpr, marker='.', label=name+', AUC = '+str(ml_auc))
        
        #calculate precision curve for each model
        from sklearn.metrics import precision_recall_curve
        from sklearn.metrics import f1_score
        from sklearn.metrics import auc

        yhat = m.predict(X_test)
        ml_precision, ml_recall, _ = precision_recall_curve(y_test, ml_probs)
        ml_f1, ml_auc = f1_score(y_test, yhat), auc(ml_recall, ml_precision)
        mls_recall.append(ml_recall)
        mls_f1.append(ml_f1)
        mls_precision.append(ml_precision)
       
        #add each precision recall curve for comparison image including all models
        plt.figure(5)
        plt.plot(ml_recall, ml_precision, marker='.', label=name+': f1=%.3f auc=%.3f' % (ml_f1, ml_auc))
        
        #create summary table for each model
        kfold = model_selection.KFold(n_splits=10, random_state=seed)
        cv_results = model_selection.cross_val_score(m, X, Y, cv=kfold, scoring=scoring)
        results.append(cv_results) 
        Ylabels = np.unique(y_test)
        if(Ylabels[0] == 1 and Ylabels[1]== 0):
            labels = reverse(labels)
        cm = confusion_matrix(y_test, y_pred, labels=Ylabels)

        
        TN = cm[0][0]
        TNs.append(TN)
        FN = cm[1][0]
        FNs.append(FN)
        TP = cm[1][1]
        TPs.append(TP)
        FP = cm[0][1]
        FPs.append(FP)
        if(FN == 0 or TN == 0):
             DOR = 0
        else:
             DOR = round((TP/FN)/(FP/TN),2)
        dor.append(DOR)
        Precision = round(metrics.precision_score(y_test, y_pred, pos_label=Yunique[0]),2)
        Precisions.append(round(Precision*100,2))
        Recall = round(metrics.recall_score(y_test, y_pred, pos_label=Yunique[0]),2)
        Recalls.append(round(Recall*100,2))
        Sensitivity = round(TP/(TP+FN),2)
        Sensitivities.append(round(Sensitivity*100,2))
        Specificity = round(TN/(TN+FP),2)
        Specificities.append(round(Specificity*100,2))
        acc = "%.2f (%.2f)" % (round((cv_results.mean()*100),2), round((cv_results.std()*100),2))
        accuracies.append(acc)
        mean = cv_results.mean()
        means.append(round(mean*100,2))        
        if len(Ylabels)==2:
            stats_text = ""
            #stats_text = "\n\nAccuracy={:0.2f}\nPrecision={:0.2f}\nRecall={:0.2f}".format(acc,Precision,Recall)
        else:
            stats_text = ""
            #stats_text = "\n\nAccuracy={:0.2f}".format(acc)
        #save confusion matrix for each model
        picturename = os.path.join(tmpdir,name+'_confusionmatrix.jpg')
        save_cm(cm, picturename, name,stats_text,decode)

        summaryTable = pd.DataFrame()   
        summaryTable[name]=['Accuracy','Recall','Precision','Sensitivity','Specificity','DOR']
        summaryTable['Value'] = [acc,Recall,Precision,Sensitivity,Specificity,DOR]
 # #       summaryTable['Method'] = [name]
 #       summaryTable['Accuracy'] = [acc]
 #        summaryTable['Recall'] = [Recall]        
 #        summaryTable['Precision'] = [Precision]
 #        summaryTable['Sensitivity'] = [Sensitivity]
 #        summaryTable['Specificity'] = [Specificity]
 #        summaryTable['DOR'] = [DOR]
        render_mpl_table(summaryTable,os.path.join(tmpdir,name+'_performance.jpg'),header_columns=0, col_width=2.0)

#         Highest_accuracy = max(means)   
        names.append(name)
          #for progressbar
        step = step + 1
        # boxplot algorithm comparison   
    popup.destroy()
    summaryTableAll = pd.DataFrame()
    summaryTableAll['Method'] = names
    summaryTableAll['Accuracy'] = accuracies
    summaryTableAll['Recall'] = Recalls        
    summaryTableAll['Precision'] = Precisions
    summaryTableAll['Sensitivity'] = Sensitivities
    summaryTableAll['Specificity'] = Specificities
    summaryTableAll['DOR'] = dor 
    render_mpl_table(summaryTableAll,os.path.join(tmpdir,'ML_performance.jpg'),header_columns=0, col_width=2.0)

 
    #plot all roc curves
#    box = ax.get_position()
#    ax.set_position([box.x0 + box.width *0.1, box.y0,
#                 box.width * 0.9, box.height])

    # Put a legend below current axis
#    ax.legend(loc='upper left', bbox_to_anchor=(0.5, -0.05),
#          fancybox=True, shadow=True, ncol=5)

    
 #   ax.xlabel('False Positive Rate')
 #   ax.ylabel('True Positive Rate')
  #  ax.title('ROC analysis')
#    ax.legend(loc='bbox_to_anchor=(1.04,1), borderaxespad=0)
    plt.figure(3)
    figROCname = os.path.join(tmpdir,'ROC_ml.jpg')
    plt.savefig(figROCname)
    plt.close(fig=figAllR)
    
    #plot all pr curves
    plt.figure(5)
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Precision-Recall analysis')
    plt.legend(bbox_to_anchor=(1.04,1), borderaxespad=0)
    figPRname = os.path.join(tmpdir,'precision_recall_ml.jpg')
    plt.savefig(figPRname)
    plt.close(fig=figAllP)

    
    figComp = plt.figure(num=6,dpi=dpi)
    figname = os.path.join(tmpdir,'comparison_plot.jpg')
    plt.title('Algorithm Comparison')
    plt.boxplot(results, patch_artist=True, showfliers=True)
#    plt.set_xticklabels(names, rotation=45)
    plt.savefig(figname, format='jpg', bbox_inches='tight')
    plt.close(fig=figComp)
    
    #save each model picutes
    for i in range(len(mls_fpr)):
        picturename = os.path.join(tmpdir,names[i]+'_roccurve.jpg')
        print(picturename)
        save_roc(mls_fpr[i], mls_tpr[i], mls_auc[i], names[i], picturename)
    
    for i in range(len(mls_recall)):    
        picturename = os.path.join(tmpdir,names[i]+'_prcurve.jpg')
        print(picturename)
        save_pr(mls_recall[i], mls_precision[i], names[i], picturename, names[i]+': f1=%.3f auc=%.3f' % (mls_f1[i], mls_auc[i]))
#todo breakpoint: save image with short feature names. export as better image, fine tuning. export image to file.
#draw decision tree
    filename = os.path.join(tmpdir,'CART_ml.sav')
    loaded_model = pickle.load(open(filename, 'rb'))  #martha
    filenameTree = os.path.join(tmpdir,'CART_ml.png')   
    filenameTreeString = os.path.join(tmpdir,'CART_ml.txt')  
    from sklearn import tree
    import pydotplus
    # Setting dpi = 300 to make image clearer than default
    ###########################################3
    #R version

    figTree = plt.figure(num=7,dpi=dpi)
    _ = tree.plot_tree(loaded_model,
           feature_names = features.tolist(), 
           class_names=("0","1"),
           filled = True, max_depth=4);

 
    figTree.savefig(filenameTree)
    plt.close(fig=figTree)

#    plt.savefig(filenameTree, format='jpg', bbox_inches='tight')
    
#    fig, axes = plt.subplots(numnrows = 1,ncols = 1,figsize = (4,4), dpi=300)
#    plt.close(fig=figComp)
#    fig.savefig(filenameTree, feature_names = features, show_weights=True)
    text_representation = tree.export_text(loaded_model, feature_names=features.tolist())
    print(text_representation)
    with open(filenameTreeString, "w") as fout:
        fout.write(text_representation)
    ###########################################
    #graphviz version
    # dot_data = tree.export_graphviz(loaded_model, out_file=None, 
    #                            feature_names=features,
    #                            class_names=("0","1"))

    # Draw graph
    # graph = pydotplus.graph_from_dot_data(dot_data)  

    # Show graph
    # Image(graph.create_png())
   # graph.write_png(filenameTree)

def render_mpl_table(data, filename, col_width=3.0, row_height=0.625, font_size=14,
                     header_color='#40466e', row_colors=['#f1f1f2', 'w'], edge_color='w',
                     bbox=[0, 0, 1, 1], header_columns=0,
                     ax=None, **kwargs):
    if ax is None:
        size = (np.array(data.shape[::-1]) + np.array([0, 1])) * np.array([col_width, row_height])
        figTab, ax = plt.subplots(figsize=size)
        ax.axis('off')
    mpl_table = ax.table(cellText=data.values, bbox=bbox, colLabels=data.columns, **kwargs)

    mpl_table.auto_set_font_size(False)
    mpl_table.set_fontsize(font_size)
    import six
    for k, cell in  six.iteritems(mpl_table._cells):
        cell.set_edgecolor(edge_color)
        if k[0] == 0 or k[1] < header_columns:
            cell.set_text_props(weight='bold', color='w')
            cell.set_facecolor(header_color)
        else:
            cell.set_facecolor(row_colors[k[0]%len(row_colors) ])
    figTab.savefig(filename)

def save_pr(ml_recall, ml_precision, name, picturename,title):
    figPR = plt.figure(dpi=dpi)
    plt.plot(ml_recall, ml_precision, marker='.', label=name)
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title(title)
    plt.savefig(picturename)
    plt.close(figPR)

def save_roc(ml_fpr, ml_tpr, ml_auc, name, picturename):     
        figRoc = plt.figure(dpi=dpi)
        plt.plot(ml_fpr, ml_tpr, marker='.', label=name)
        plt.title(name+': ROC AUC=%.3f' % (ml_auc))
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.savefig(picturename)
        plt.close(figRoc)
        
def save_cm(cm, figname, clname, stats_text,labels):
    import seaborn as sn
 
    cm_sum = np.sum(cm, axis=1, keepdims=True)
    cm_perc = cm / cm_sum.astype(float) * 100
    annot = np.empty_like(cm).astype(str)
    nrows, ncols = cm.shape
    for i in range(nrows):
        for j in range(ncols):
            c = cm[i, j]
            p = cm_perc[i, j]
            if i == j:
                s = cm_sum[i]
                annot[i, j] = '%.1f%%\n%d/%d' % (p, c, s)
            elif c == 0:
                annot[i, j] = ''
            else:
                annot[i, j] = '%.1f%%\n%d' % (p, c)
    cmdf = pd.DataFrame(cm, index=labels, columns=labels)
    cmdf.index.name = 'Actual'
    cmdf.columns.name = 'Predicted' + stats_text
    figCM = plt.figure(dpi=dpi)
    figCM, ax = plt.subplots(figsize=(fx,fy))
    group_names = ['True Neg','False Pos','False Neg','True Pos']
    sns.heatmap(cmdf, cmap= 'binary', annot=annot, fmt='', ax=ax)    
    plt.title("Confusion matrix for " + clname)
    figCM.savefig(figname,format='jpg', bbox_inches='tight')
    plt.close(figCM)


def getBestFeature(root, numberToShow, loaded_model):
    import numpy as np
    import pandas as pd
    global tmpdir
    
    tableDF = table.to_dataframe(dense=1)

    #todo generic, such that other will be implemented too
    features = np.concatenate((tableDF.index.values, diversityDF.columns.values)).tolist()

    importance = loaded_model.coef_[0]
    
    classpred = []
    for x in importance:
        if(x > 0):
            classpred.append("class 1")
        else:
            classpred.append("class 0")

    featureScores = {'Feature':features,
                     'Score':np.absolute(importance),
                     'Class':classpred}

    df = pd.DataFrame(featureScores)

    #print sorted table of n = 20
    featureScoresN = df.nlargest(numberToShow,'Score')    
    return(featureScoresN.to_dict('index'))
    
    
def taxaChi2(root, numberToShow):
    import numpy as np
    import pandas as pd
    global Location
    global featureScoresN
    global shannonAll
    
    #prepare data
    tableDF = table.to_dataframe(dense=1).transpose()

    chiDF = pd.concat([diversityDF, tableDF], axis=1)
    
    #todo generic, such that other will be implemented too
    tabledfrownames = chiDF.index.values
    overlapIdsorted = [s for s in tabledfrownames if s in mdata.index.values]    
    mdata_new = mdata.loc[overlapIdsorted] 
    Y = mdata_new.iloc[:,Location].values
    Yunique = np.unique(Y)

    #apply SelectKBest class to extract N best features
    bestfeatures = SelectKBest(score_func=chi2, k='all')

    fit = bestfeatures.fit(chiDF,Y)      
    ymeans = {}
    extracolumn = ['Names','Score','P-value']
    for varY in Yunique:
        ymeans[varY]=[]
        extracolumn.append("mean."+varY)
        
    for taxon in chiDF.columns.values:
        dftmp = pd.DataFrame(data = {'t' : chiDF[taxon], 'var':Y})
        varmeans = round(dftmp.groupby('var').mean(),2)

        for varY in Yunique:
            ymeans[varY].append(varmeans.loc[varY,'t'])
        
    dfnames = pd.DataFrame(chiDF.columns.values)
    dfscores = pd.DataFrame(np.round(fit.scores_,2))
    dfpvalues = pd.DataFrame(np.round(fit.pvalues_,2))
    dfmean = pd.DataFrame(data=ymeans) 
    featureScores = pd.concat([dfnames,dfscores,dfpvalues,dfmean],axis=1)
    featureScores.columns = extracolumn
    #featureScoresN panda dataframe for export
    featureScoresN = featureScores.nlargest(numberToShow,'Score')
    return(featureScoresN.to_dict('index'))

    
                   
def browse_file2():
    import xlsxwriter
    # Create an new Excel file and add a worksheet.
    workbook = xlsxwriter.Workbook('ResultsResults.xlsx')
    worksheet1 = workbook.add_worksheet('Box Plot')
    #worksheet2 = workbook.add_worksheet('Algorithm Evaluation')
    #worksheet3 = workbook.add_worksheet('Important Features')
    # Widen the first column to make the text clearer.
    worksheet1.set_column('A:A', 30)
    # Insert an image.
    worksheet1.write('A1', 'Results of Machine Learning algorithms')
    worksheet1.write('A2', 'Box Plot:')
    worksheet1.insert_image('B2', 'Comparative Analysis.jpg' )
    #path = "C:/Users/apurvaK/ResultsResults.xlsx"
    #worksheet2.write('A1', 'Results of Machine Learning algorithms')
    writer = pd.ExcelWriter('ResultsResults.xlsx', engine = 'xlsxwriter')
    workbook=writer.book
    worksheet=workbook.add_worksheet('Algorithm Evaluation')
    writer.sheets['Algorithm Evaluation'] = worksheet
    #worksheet.write_string(0, 0, df)
    df.to_excel(writer,sheet_name='Algorithm Evaluation',startrow=0 , startcol=0)
    worksheet=workbook.add_worksheet('Important Features')
    writer.sheets['Important Features'] = worksheet
    #worksheet.write_string(0, 0, df)
    n.to_excel(writer,sheet_name='Important Features',startrow=0 , startcol=0)
    worksheet = workbook.add_worksheet('Box Plot')
    writer.sheets['Box Plot'] = worksheet
    worksheet.set_column('A:A', 30)
    # Insert an image.
    worksheet.write('A1', 'Results of Machine Learning algorithms')
    worksheet.write('A2', 'Box Plot:')
    worksheet.insert_image('B2', 'Comparative Analysis.jpg' )
    #df.to_excel(writer, 'Sheet2', index = False)
    #n.to_excel(writer, 'Sheet3', index = False)  
    #writer.save()
    #writer.close()
    workbook.close()
    

    
    
def browse_file3():
    
    global df
    #global table
    
    #table = PrettyTable()
    #table.padding_width = 1 
    algo = ['LR','LDA','KNN','KNN - 25','CART','NB','SVM - Poly', 'SVM - Linear', 'SVM - Sigmoid','SVM - RBF']
    column_names = ["Algorithm","Accuracy", "Sensitivity", "Specificity", "TP", "TN", "FP", "FN", "Diagnostic Odds Ratio", "Precision",]
    #table.add_column(column_names[0],algo)
    #table.add_column(column_names[1],means)
    #table.add_column(column_names[2],Precisions)
    #table.add_column(column_names[3],Recalls)
    #table.add_column(column_names[4],TPs)
    #table.add_column(column_names[5],TNs)
    #table.add_column(column_names[6],FPs)
    #table.add_column(column_names[7],FNs)
   # table.add_column(column_names[8],Sensitivities)
   # table.add_column(column_names[9],Specificities)
    

    df = pd.DataFrame(list(zip(algo, means, Sensitivities, Specificities, TPs, TNs, FPs, FNs, dor, Precisions)), 
               columns = column_names) 
    df.sort_values(by=['Accuracy'], ascending=False)
    #render dataframe as html
    html = df.to_html()

    #write html to file
    text_file = open("Save results.html", "w")
    text_file.write(html)
    text_file.close()
    webbrowser.open_new_tab('Save results.html')
   
   
    #showinfo('Algorithm Evaluation', format(df))
    #table.add_column(column_names[10],images)  
    #table.align = "l"
    #table.sortby = "Accuracy"
    ##table.font = "Consolas"
    #showinfo('Algorithm Evaluation', table)
    #with open('file.txt', 'w') as w:
        #w.write(str(table))
    #df = pd.read_table('C:/Users/apurvaK/file.txt', sep='|')
    #data = []
    #with open(file) as f:
       # for line in f:
            #if '-' not in line:
               # data.append(line.split('|')[1:-1])
   # print(data)


class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = Tk.Label(self.tw, text=self.text, justify='left',
                       background='white', relief='solid', borderwidth=1,
                       font=("times", "8", "normal"))
        label.pack(ipadx=1)
    def close(self, event=None):
        if self.tw:
            self.tw.destroy()
            
class Wizard(Tk.Tk):

    def __init__(self, *args, **kwargs):
        
        Tk.Tk.__init__(self, *args, **kwargs)
        global frame

        container = Tk.Frame(self)
        container.configure(bg='red')
        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
    


        self.frames = {}

#        for F in (logisticRegression, summary):
        for F in (Home, PageOne, PageTwo, PageThree, PageFour, PageFive,mostAbundant,logisticRegression, lda, knn,summary, cart):      
            self.frame = F(container, self, tmpdir)
            self.frame.configure(bg=BG_COLOR)
            self.frame.grid(row=0,column=0,sticky="nsew")
            self.frames[F] =self.frame         
            
 
    
        #self.show_frame(PageOne)#for testing
        if(debug):
            #examplefile()
            examplefile2()
            self.show_frame(Home)
        else:
            self.show_frame(PageOne)
            
    def calculateModel(self,name, model):
        print("calculate model: "+name)
        global tableDF
        global Location
        global table
        tableDF = table.to_dataframe(dense=1).transpose()        
        tableDF = table.to_dataframe(dense=1)
        X = pd.concat([diversityDF, tableDF.transpose()], axis=1)    

        tabledfrownames = X.index.values
        overlapIdsorted = [s for s in tabledfrownames if s in mdata.index.values]    
        mdata_new = mdata.loc[overlapIdsorted]
        Y = mdata_new.iloc[:,Location].values
        Yunique = np.unique(Y)
        decode = Yunique
        if(len(Yunique) == 2 and not((Yunique[0]==0 and Yunique[1]==1) or (Yunique[0]==1 and Yunique[1]==0) )):
            Y[ Y==Yunique[0]] = 0
            Y[ Y==Yunique[1]] = 1
            Y=Y.astype('int')
            Yunique = [0,1] 
        
        seed = 7
        global Highest_accuracy 
        results = []
        names = []
        global means
        means = []
        cnf_matrices = []
        global TNs
        TNs = []
        global TPs
        TPs = []
        global FPs
        FPs = []
        global FNs
        FNs = []
        global Precisions
        Precisions = []
        global Recalls
        Recalls = []
        global Sensitivities
        Sensitivities = []
        global Specificities
        Specificities = []
        global accuracies 
        accuracies = []
        global images
        images = []
        global itg
        itg = []
        global dor
        dor = []
        scoring = 'accuracy'

        global tmpdir
        

        from sklearn.model_selection import train_test_split
        X_train,X_test,y_train,y_test=train_test_split(X,Y,test_size=0.3,random_state=0)
        m = model

        #fit the model with data
        m.fit(X_train,y_train)
        
        # save the model to disk
        filename = os.path.join(tmpdir,name+'_ml.sav')
        pickle.dump(m, open(filename, 'wb'))
        y_pred=m.predict(X_test)
        print("saving model to disk")
         
        #save confusion matrix for each model
        picturename = os.path.join(tmpdir,name+'_confusionmatrix.jpg')

        cm = confusion_matrix(y_test, y_pred)
        save_cm(cm, picturename, name, "", decode)
        print("saving confusion matrix")
        #save roc curve for each model
        from sklearn.metrics import roc_curve
        from sklearn.metrics import roc_auc_score  
        picturename = os.path.join(tmpdir,name+'_roccurve.jpg')
        ml_probs = m.predict_proba(X_test)
        ml_probs = ml_probs[:, 1]
        ml_auc = round(roc_auc_score(y_test, ml_probs),3)
        ml_fpr, ml_tpr, _ = roc_curve(y_test, ml_probs)
        save_roc(ml_fpr, ml_tpr, ml_auc, name, picturename)
            

    
        #save precision curve for each model
        from sklearn.metrics import precision_recall_curve
        from sklearn.metrics import f1_score
        from sklearn.metrics import auc
        picturename = os.path.join(tmpdir,name+'_prcurve.jpg')

        yhat = m.predict(X_test)
        ml_precision, ml_recall, _ = precision_recall_curve(y_test, ml_probs)
        ml_f1, ml_auc = f1_score(y_test, yhat), auc(ml_recall, ml_precision)
        save_pr(ml_recall, ml_precision, name, picturename, name+': f1=%.3f auc=%.3f' % (ml_f1, ml_auc))
       
        #create summary table for each model
        kfold = model_selection.KFold(n_splits=10, random_state=seed)
        cv_results = model_selection.cross_val_score(m, X, Y, cv=kfold, scoring=scoring)
        results.append(cv_results)   
        cm = confusion_matrix(y_test, y_pred) 
        TN = cm[0][0]
        TNs.append(TN)
        FN = cm[1][0]
        FNs.append(FN)
        TP = cm[1][1]
        TPs.append(TP)
        FP = cm[0][1]
        FPs.append(FP)
        if(FN == 0 or TN == 0):
             DOR = 0
        else:
             DOR = round((TP/FN)/(FP/TN),2)
        Precision = round(metrics.precision_score(y_test, y_pred, pos_label=Yunique[0]),2)
        Precisions.append(round(Precision*100,2))
        Recall = round(metrics.recall_score(y_test, y_pred, pos_label=Yunique[0]),2)
        Sensitivity = round(TP/(TP+FN),2)
        Sensitivities.append(round(Sensitivity*100,2))
        Specificity = round(TN/(TN+FP),2)
        Specificities.append(round(Specificity*100,2))
        acc = "%.2f (%.2f)" % (round((cv_results.mean()*100),2), round((cv_results.std()*100),2))
        summaryTable = pd.DataFrame() 
        summaryTable[name]=['Accuracy','Recall','Precision','Sensitivity','Specificity','DOR']
        summaryTable['Value'] = [acc,Recall,Precision,Sensitivity,Specificity,DOR]

        render_mpl_table(summaryTable,os.path.join(tmpdir,name+'_performance.jpg'),header_columns=0, col_width=2.0)

                
        
    def show_frame(self, cont, what=1):
        frame = self.frames[cont]
        frame.tkraise()
        print(cont)
        if(cont == PageFive):
            transferData()
        elif(cont == Home or cont == mostAbundant or cont == logisticRegression or cont == lda or cont == summary or cont == knn or cont == cart):
            menubar = Tk.Menu(self)
            pageMenu = Tk.Menu(menubar)
            pageMenu.add_command(label="Change varible")
            pageMenu.add_command(label="Change outcome")
            pageMenu.add_command(label="Remove samples")
            menubar.add_cascade(label="Settings", menu=pageMenu) 
            
            pageMenu2 = Tk.Menu(menubar)           
            pageMenu2.add_command(label="Summary",command=lambda: self.show_frame(summary))
            pageMenu2.add_command(label="Logistic Regression",command=lambda: self.show_frame(logisticRegression))
            pageMenu2.add_command(label="Linear Discriminant Analysis",command=lambda: self.show_frame(lda))
            pageMenu2.add_command(label="KNN",command=lambda: self.show_frame(knn,cont))
            pageMenu2.add_command(label="CART",command=lambda: self.show_frame(cart, cont))
            pageMenu2.add_command(label="NB",command=lambda: self.show_frame(nb))
            pageMenu2.add_command(label="SVM",command=lambda: self.show_frame(svm))
            pageMenu2.add_command(label="xgboost",command=lambda: self.show_frame(xgboost))
            pageMenu2.add_command(label="Elastic Net",command=lambda: self.show_frame(elasticnet))   

            menubar.add_cascade(label="Machine Learning", menu=pageMenu2) 
            
            pageMenu3 = Tk.Menu(menubar)
            pageMenu3.add_command(label="Chi-Square",command=lambda: self.show_frame(mostAbundant))
            menubar.add_cascade(label="Feature selection", menu=pageMenu3)
    
            pageMenu4 = Tk.Menu(menubar)
            pageMenu4.add_command(label="blubb")
            pageMenu4.add_command(label="Support Vector")
            pageMenu4.add_command(label="Random Forest")
            menubar.add_cascade(label="Predict", menu=pageMenu4)       
            self.config(menu=menubar)   
            if(cont == Home):
                print('con == Home')
                calculateModels(self,what=1)  
            elif(cont == mostAbundant):
                global homeTable
                global mostAbundantErrVar
                try:
                    integer_result = int(mostAbundantVar.get())
                except ValueError:
                    mostAbundantErrVar.set("Please enter a number.")
                else:
                    if(integer_result > 100):
                        mostAbundantErrVar.set("Table restricted to 100 elements.") 
                    else:
                        mostAbundantErrVar.set("")                     
                        bestFeature=taxaChi2(self, integer_result)  
                        model = homeTable.model
                        model.deleteRows()
                        model.importDict(bestFeature)  
                        homeTable.redraw()
                        exportButton.grid()
            elif(cont == summary):
                print('cont == summary')
                global summaryAlabel4
                global summaryAlabel3
                global summaryAlabel2
                global summaryAlabel
                
                figureSum1 = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'precision_recall_ml.jpg')))
                summaryAlabel.configure(image=figureSum1)
                summaryAlabel.image = figureSum1
                
                figureSum2 = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'ROC_ml.jpg')))
                summaryAlabel2.configure(image=figureSum2)
                summaryAlabel2.image = figureSum2

                figureSum3 = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'comparison_plot.jpg')))
                summaryAlabel3.configure(image=figureSum3)
                summaryAlabel3.image = figureSum3 
                
                figureSum4 = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'ML_performance.jpg')))
                summaryAlabel4.configure(image=figureSum4)
                summaryAlabel4.image = figureSum4
                
            elif(cont == logisticRegression):
                global lrFeatureTable
                global lrFeatureTable
                global lrfeaturesNVar 
                try:
                    integer_result = int(lrfeaturesNVar.get())
                except ValueError:
                     lrErrVar.set("Please enter a number.")
                else:
                    if(integer_result > 100):
                         lrErrVar.set("Table restricted to 100 elements.") 
                    else:
                         lrErrVar.set("")  
                         if(what == 2):  #what is to save the model, 2: y, 1 no
                             self.calculateModel('LR', LogisticRegression(C=float(tuningCVar.get()),solver=tuningSolverVar.get()))
                         filename = os.path.join(tmpdir,'LR_ml.sav')
                         loaded_model = pickle.load(open(filename, 'rb'))
                         bestFeature=getBestFeature(self, integer_result,loaded_model)  
                         model = lrFeatureTable.model
                         model.deleteRows()
                         model.importDict(bestFeature)  
                         lrFeatureTable.redraw()
                         exportButton.grid()
                         global lrroclabel
                         figureROC = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_roccurve.jpg')))
                         lrroclabel.configure(image=figureROC)
                         lrroclabel.image = figureROC
                         global lrprlabel
                         lrfigurePR = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_prcurve.jpg')))
                         lrprlabel.configure(image=lrfigurePR)
                         lrprlabel.image = lrfigurePR
                         global lrperformancelabel
                         lrfigurePerformance = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_performance.jpg')))
                         lrperformancelabel.configure(image=lrfigurePerformance)
                         lrperformancelabel.image = lrfigurePerformance
                         global lrcmlabel
                         lrfigureCM = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_confusionmatrix.jpg')))
                         lrcmlabel.configure(image=lrfigureCM)
                         lrcmlabel.image = lrfigureCM
            elif(cont == lda):
                global ldaFeatureTable
                global ldaErrVar
                global ldafeaturesNVar 
                try:
                    integer_result = int(ldafeaturesNVar.get())
                except ValueError:
                     ldaErrVar.set("Please enter a number.")
                else:
                    if(integer_result > 100):
                         ldaErrVar.set("Table restricted to 100 elements.") 
                    else:
                         ldaErrVar.set("")  
                         if(what == 2):  #what is to save the model, 2: y, 1 no
                             if(LDAtuningShrinkVar.get()=="float"):
                                 if(float(LDAtuningShrink2Var.get()) >=0 and float(LDAtuningShrink2Var.get())<=1):
                                     self.calculateModel('LDA', LDA(shrinkage=float(LDAtuningShrink2Var.get()),solver=LDAtuningSolverVar.get()))
                                     ldaErrVar.set("")    
                                 else:
                                     ldaErrVar.set("Shrinkage value is a float between 0 and 1.")     
                             else:
                                 if(LDAtuningShrinkVar.get()=="none" or LDAtuningShrinkVar.get()=="auto"):
                                     self.calculateModel('LDA', LDA(shrinkage=float(LDAtuningShrink2Var),solver=LDAtuningSolverVar.get()))

                         filename = os.path.join(tmpdir,'LDA_ml.sav')
                         loaded_model = pickle.load(open(filename, 'rb'))  #martha
                         bestFeature=getBestFeature(self, integer_result,loaded_model)  
                         model = ldaFeatureTable.model
                         model.deleteRows()
                         model.importDict(bestFeature)  
                         ldaFeatureTable.redraw()
                         exportButton.grid()
                         global ldaroclabel
                         ldafigureROC = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_roccurve.jpg')))
                         ldaroclabel.configure(image=ldafigureROC)
                         ldaroclabel.image = ldafigureROC
                         global ldaprlabel
                         ldafigurePR = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_prcurve.jpg')))
                         ldaprlabel.configure(image=ldafigurePR)
                         ldaprlabel.image = ldafigurePR
                         global ldaperformancelabel
                         ldafigurePerformance = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_performance.jpg')))
                         ldaperformancelabel.configure(image=ldafigurePerformance)
                         ldaperformancelabel.image = ldafigurePerformance
                         global ldacmlabel
                         ldafigureCM = ImageTk.PhotoImage(Image.open( os.path.join(tmpdir,'LR_confusionmatrix.jpg')))
                         ldacmlabel.configure(image=ldafigureCM)
                         ldacmlabel.image = ldafigureCM                         
            elif(cont == knn):
                    knn.show_frame(frame,what)
            elif(cont == cart):
                    tableDF = table.to_dataframe(dense=1)
                    features = np.concatenate((tableDF.index.values, diversityDF.columns.values)).tolist()
                    cart.show_frame(frame,what, features)
                          
def diversity():
    global shannonAll
    global diversityDF
    tableDF = tableRel.to_dataframe(dense=1)
    diversityDF = pd.DataFrame( )
    #choices4 = {"None", "All", "Shannon diversity", "Simpson diversity", "InvSimpson diversity","Richness"}
    diversityIndices = []
    
    if(diversitySelection.get() == "All"):
        diversityIndices.append(shannon(tableDF))
        diversityIndices.append(simpson(tableDF))  
        diversityIndices.append(invSimpson(tableDF)) 
        diversityIndices.append(shannon(tableDF))
        #diversityIndices.append(richness(table.to_dataframe(dense=1)))      #todo uncomment
        print("to make it fast richness was replaced by shannon")
        diversityDF = pd.DataFrame(diversityIndices)
        diversityDF = diversityDF.transpose()
        diversityDF.columns = ['shannon','simpson','invSimpson','richness']
       # diversityDF.reset_index(list(tableDF.columns) )  #todo? add this column below
    elif(diversitySelection.get() == "Shannon diversity"):
        diversityIndices.append(shannon(tableDF))
        diversityDF = pd.DataFrame(diversityIndices)
        diversityDF = diversityDF.transpose()
        diversityDF.columns = ['shannon']
    elif(diversitySelection.get() == "Simpson diversity"):
        diversityIndices.append(simpson(tableDF))
        diversityDF = pd.DataFrame(diversityIndices)
        diversityDF = diversityDF.transpose()
        diversityDF.columns = ['simpson']
    elif(diversitySelection.get() == "InvSimpson diversity"):
        diversityIndices.append(invSimpson(tableDF))
        diversityDF = pd.DataFrame(diversityIndices)
        diversityDF = diversityDF.transpose()
        diversityDF.columns = ['invSimpson']
    elif(diversitySelection.get() == "Richness"):
        diversityIndices.append(richness(table.to_dataframe(dense=1)))  
        diversityDF = pd.DataFrame(diversityIndices)
        diversityDF = diversityDF.transpose()
        diversityDF.columns = ['richness']
    else:
        print("warning index not defined")
    diversityDF.index = list(tableDF.columns)
    print(diversityDF)


def richness(countsDF):
        diversity = []
        sampleSizes =  list()
        for sample in list(countsDF):
                sampleCounts = countsDF[sample]
                newlist = [int(float(i)) for i in sampleCounts ]
                sampleTotal = sum(newlist)
                sampleSizes.append(sampleTotal)
                
        minRarefy = min(sampleSizes)
        print('samples rarefied to ',minRarefy)
        for sample in list(countsDF):
                sampleCounts = countsDF[sample]
                newList0 = [ int(float(i)) for i in sampleCounts if float(i) > 0]
                J = sum(newList0)
   
                ldiv = math.log(nCk(J,minRarefy))
                p1 = list()
                for i in newList0:

                        if((J - float(i)) < minRarefy):
                                p1.append(0)  #todo I had 1 in first but in R vegan it is 0
                        else:
                                val = 1-(math.exp(math.log(nCk((J-int(float(i))),minRarefy)) - ldiv))
                                p1.append(val)
                diversity.append(sum(p1))
        return(diversity)


def nCk(n,k):
        m=0
        op1 = 0
        num = 0
        dem = 1
        if k==0:
                m=1
        if k==1:
                m=n
        if k>=2:
                num,dem,op1,op2=1,1,k,n
        while(op1>=1):
                num*=op2
                dem*=op1
                op1-=1
                op2-=1
        m=num // dem
        return m


def simpson(countsDF):
        diversity = []
        for sample in list(countsDF):
                sampleCounts = countsDF[sample]        
                simpson = 0
                pTest = 0
                for p in sampleCounts:
                        if(p > 0):
                                simpson += p**2
                                pTest += p
                diversity.append(1 - simpson)
                if(round(pTest) != 1):
                        print('Shannon requirement not valid: sum(pi) is not 1 but ',pTest)
        return(diversity)


def invSimpson(countsDF):
        diversity = []
        for sample in list(countsDF):
                sampleCounts = countsDF[sample]   
                simpson = 0
                pTest = 0
                for p in sampleCounts:
                        if(p > 0):
                                simpson += p**2
                                pTest += p
                diversity.append(1 - simpson)
                if(round(pTest) != 1):
                        print('Shannon requirement not valid: sum(pi) is not 1 but ',pTest)  
        return(diversity)
    
    
def shannon(countsDF):
    #shannon
    shannonAll = []
    for sample in list(countsDF):
            shannon = 0
            pTest = 0
            sampleCounts = countsDF[sample]
            for p in sampleCounts:
                if(p > 0):
                    shannon += p * math.log(p)
                    pTest += p
            shannon2 = sum(calcP(n) for n in sampleCounts if n > 0)
            shannonAll.append(shannon * -1)
            if(round(pTest) != 1):
                print('Shannon requirement not valid: sum(pi) is not 1 but ',pTest)
    return(shannonAll)
  
def calcP(n):
        if(n is 0):
                return 0
        else:
                return (float(n) * math.log(float(n)))    
            
def transferData():
    warning = filterMin()
    diversity()    #todo 
    print(warning)
 
  
class Home(Tk.Frame):

    def __init__(self, parent, controller, tmpdir):
        global homeLabelFig
        global menubar
        global homeTable
        global resultFrame
        Tk.Frame.__init__(self,parent)
        
        label = Tk.Label(self, text="Alora Analysis", font=LARGE_FONT,bg=BG_COLOR)
        label.grid(pady=10,padx=10,column=0,row=0,columnspan=2)
        
        #path = "images/fileerror.png"
        path = 'images\Alora - Data Analytics tool.jpg'

        im = ImageTk.PhotoImage(Image.open(path))
#        homeLabelFig = Tk.Label(self, image=im)
#        homeLabelFig.pack()

        homeVarText= StringVar()
        homeLabelText = Tk.Label(self, textvariable=homeVarText,fg="red",bg=BG_COLOR)
        homeLabelText.grid(row=1, column=0,columnspan=2,pady=10,padx=10) 



        
        button2 = Tk.Button(self, text="Exit",
                            command=lambda: controller.show_frame(PageTwo))
        button2.grid(row=4,column=2)      


class mostAbundant(Tk.Frame):

    def __init__(self, parent, controller,tmpdir):
        global homeLabelFig
        global homeTable
        global mostAbundantVar
        global mostAbundantErrVar
        global exportButton
        Tk.Frame.__init__(self,parent)
        
        label = Tk.Label(self, text="Best features", font=LARGE_FONT,bg=BG_COLOR)
        label.grid(row=0,pady=10,columnspan=3,padx=10)
                
        mostAbundantErrVar = StringVar("")        
        mostAbundantError = Tk.Label(self,textvariable=mostAbundantErrVar,fg="red",bg=BG_COLOR)
        mostAbundantError.grid(row=1, column=0, columnspan=3, pady=2,padx=2)   

        mostAbundantDes = Tk.Label(self, text="Select number of taxa to display with the hightest score",bg=BG_COLOR)
        mostAbundantDes.grid(row=2, column=0,sticky=W,padx = 10,pady=1)
        
        mostAbundantVar = StringVar(self, value="10")   
        mostAbundantEntry = Tk.Entry(self, width=5,textvariable=mostAbundantVar)
        mostAbundantEntry.grid(row=2,column=1, sticky=W)
        mostAbundantTT = CreateToolTip(mostAbundantEntry,"Number of most abundant taxa")
   

        mostAbundantButton = Tk.Button(self, text = 'Show', command=lambda: controller.show_frame(mostAbundant), bg='blue', fg='white')
        mostAbundantButton.grid(row=2, column=2,sticky=W)         
        #path = "images/fileerror.png"
        path = 'images\Alora - Data Analytics tool.jpg'

        im = ImageTk.PhotoImage(Image.open(path))
#        homeLabelFig = Tk.Label(self, image=im)
#        homeLabelFig.pack()
      
        resultFrame = Frame(self)
        resultFrame.grid(row=3,columnspan=3)  
        data = {'rec1': {'Names': 'test', 'Score': 108.79, 'P-value': 0},'rect2': {'Names':'test2','Score': 108.79, 'P-value': 0}} 

        homeTable = TableCanvas(resultFrame, data = data,
                                editable=False,read_only=True,
                                reverseorder=1)
        homeTable.show()  

        # Create a Tkinter variable
        global exportTableVar
        exportTableVar = StringVar(self)

        # Dictionary with options
        choices = {"comma seperated - CSV", 
                   "tab seperated - TSV",
                   "excel file - XLSX" }
        exportTableVar.set('comma seperated - CSV') # set the default option

        exportPopupMenu = Tk.OptionMenu(self, exportTableVar, *choices)
        exportPopupMenu.grid(row = 4, column = 0,padx = 2, pady=1,sticky=E)

        exportButton = Tk.Button(self, text = 'Export table', command=browse_export_feature, fg = 'white', bg = 'purple')
        exportButton.grid(row=4,column=1,sticky=W)

        # broButton.grid(row=4, column=1)
        #broButton = Tk.Button(self, text = 'Save Results', width = 30, command=browse_file2, bg = 'white', fg = 'blue')
        #broButton.pack()
    # broButton.grid(row=8, column=1)
        #broButton = Tk.Button(self, text = 'Algorithm Evaluation', width = 30, command=browse_file3, bg='blue', fg='white')
        #broButton.pack()
    # broButton.grid(row=6, column=1)

        #button = Tk.Button(self, text="Save",
        #                    command=lambda: controller.show_frame(PageOne))
        #button.pack()

class summary(Tk.Frame):

    def __init__(self, parent, controller,tmpdir):
        print("summary frame")
        global canvas
        global frame_results
        Tk.Frame.__init__(self,parent)


        canvas = Tk.Canvas(self,borderwidth=0, background=BG_COLOR, width=WIDTH, height=HEIGHT,scrollregion=(0,0,WIDTH+200,HEIGHT+500))
        #container.grid(row = 1, column = 0, sticky = "nsew")
        #container.pack(side="top", fill="both", expand=True)
        canvas.grid_rowconfigure(0, weight=1)
        canvas.grid_columnconfigure(0, weight=1)
        
        scrollbar = Tk.Scrollbar(self, orient="vertical")
        scrollbar.pack(fill = Y, side = RIGHT, expand = FALSE)
        scrollbar.config(command=canvas.yview)
        
        scrollbarx = Tk.Scrollbar(self, orient="horizontal")
        scrollbarx.pack(fill = X, side = BOTTOM, expand = FALSE)
        scrollbarx.config(command=canvas.xview)    
        canvas.configure(xscrollcommand = scrollbarx.set,yscrollcommand=scrollbar.set)
        canvas.config(width=WIDTH, height=HEIGHT)
        canvas.pack(side=LEFT, expand=True, fill = BOTH)

        frame_results = Tk.Frame(canvas, bg=BG_COLOR)
        canvas_frame = canvas.create_window((0, 0), window=frame_results, anchor='nw', tags="frame")

        frame_results.bind("<Configure>", self.onFrameConfigure)
        canvas.bind('<Configure>', self.frameSize)
        
        global summaryAlabel
        figureROC = ImageTk.PhotoImage(Image.open("images/fileerror.png"))
        summaryAlabel = Tk.Label(frame_results)
        summaryAlabel.image = figureROC # keep a reference!
        summaryAlabel.grid(row=1, column=0)
        global summaryAlabel2
        summaryAlabel2 = Tk.Label(frame_results)
        summaryAlabel2.image = figureROC # keep a reference!
        summaryAlabel2.grid(row=1, column=1)
        global summaryAlabel3
        summaryAlabel3 = Tk.Label(frame_results)
        summaryAlabel3.image = figureROC # keep a reference!
        summaryAlabel3.grid(row=2, column=0)        
 
        global summaryAlabel4
        summaryAlabel4 = Tk.Label(frame_results)
        summaryAlabel4.image = figureROC # keep a reference!
        summaryAlabel4.grid(row=3, column=0)
        
    def onFrameConfigure(self, event):
        canvas.configure(scrollregion=canvas.bbox("all"))

    def frameSize(self, event):
        canvas_width = event.width
        canvas_height = event.height
        canvas.itemconfig(frame_results, width = canvas_width, height = canvas_height)   
        Tk.Frame.__init__(self,parent)
def on_configure(event):
    # update scrollregion after starting 'mainloop'
    # when all widgets are in canvas
    container.configure(scrollregion=container.bbox('all'))
    
class lda(Tk.Frame):

    def __init__(self, parent, controller,tmpdir):
        print("Linear Discriminant Analysis")
        global mlLabelFig
        global ldaFeatureTable
        global ldaErrVar   
        global exportButton
        global ldafeaturesNVar
        global canvas
        global frame_results
        Tk.Frame.__init__(self,parent)


        canvas = Tk.Canvas(self,borderwidth=0, background=BG_COLOR, width=WIDTH, height=HEIGHT,scrollregion=(0,0,WIDTH+200,HEIGHT+500))
        #container.grid(row = 1, column = 0, sticky = "nsew")
        #container.pack(side="top", fill="both", expand=True)
        canvas.grid_rowconfigure(0, weight=1)
        canvas.grid_columnconfigure(0, weight=1)
        
        scrollbar = Tk.Scrollbar(self, orient="vertical")
        scrollbar.pack(fill = Y, side = RIGHT, expand = FALSE)
        scrollbar.config(command=canvas.yview)
        
        scrollbarx = Tk.Scrollbar(self, orient="horizontal")
        scrollbarx.pack(fill = X, side = BOTTOM, expand = FALSE)
        scrollbarx.config(command=canvas.xview)    
        canvas.configure(xscrollcommand = scrollbarx.set,yscrollcommand=scrollbar.set)
        canvas.config(width=WIDTH, height=HEIGHT)

#        canvas_frame = self.canvas.create_window((0,0), window=self, anchor="nw", tags="frame")
        canvas.pack(side=LEFT, expand=True, fill = BOTH)

        frame_results = Tk.Frame(canvas, bg=BG_COLOR)
        canvas_frame = canvas.create_window((0, 0), window=frame_results, anchor='nw', tags="frame")

        frame_results.bind("<Configure>", self.onFrameConfigure)
        canvas.bind('<Configure>', self.frameSize)


        label = Tk.Label(frame_results, text="Linear Discriminant Analysis", font=LARGE_FONT,bg=BG_COLOR)
        label.grid(row=0,pady=10,columnspan=6,padx=10)
                
        ldaErrVar = StringVar("")        
        ldaError = Tk.Label(frame_results,textvariable=ldaErrVar,fg="red",bg=BG_COLOR)
        ldaError.grid(row=1, column=0, columnspan=6, pady=2,padx=2)   

        if( advanced == 1):
            tuningFrame = Tk.LabelFrame(frame_results, text = "Tuning", padx=10, pady=10)
            tuningFrame.grid(row=2, columnspan = 6, sticky=W, padx=2)

            global LDAtuningSolverVar
            global LDAtuningShrinkVar
            global LDAtuningShrink2Var
            tuningSolverDes = Tk.Label(tuningFrame, text="Select solver")
            tuningSolverDes.grid(row=3, column=0,sticky=W,padx = 10,pady=1)
            solverVarChoices = {"svd", "lsqr", "eigen"}
            LDAtuningSolverVar = StringVar(tuningFrame, value="svd")                       
            #tuningSolverVar.set('lbfgs') # set the default option
            tuningSolverPopup = Tk.OptionMenu(tuningFrame, LDAtuningSolverVar, *solverVarChoices)
            tuningSolverPopup.grid(row=3,column=1, sticky=W)
            tuningCTT = CreateToolTip(tuningSolverPopup,"Solver to use. SVD is recommended for data with many features.")

            tuningShrinkDes = Tk.Label(tuningFrame, text="Select shrinkage for lsqr and eigen solvers.")
            tuningShrinkDes.grid(row=4, column=0,sticky=W,padx = 10,pady=1)
            shrinkVarChoices = [ ("None", "none"), ("Auto", "auto"),("Float","float")]
            LDAtuningShrinkVar = StringVar(tuningFrame, value="none")
            col=1
            for label, mode in shrinkVarChoices:
                rb = Radiobutton(tuningFrame, text=label, variable=LDAtuningShrinkVar, value=mode)
                rb.grid(row=4,column=col)
                col=col+1
            LDAtuningShrink2Var = StringVar(tuningFrame, value="0")   
            tuningShrinkEntry = Tk.Entry(tuningFrame, width=5,textvariable=LDAtuningShrink2Var)
            tuningShrinkEntry.grid(row=4,column=col, sticky=W)
            tuningShrinkTT = CreateToolTip(tuningShrinkEntry,"Must be a float between 0 and 1 (fixed shrinkage parameter)")
   

            featureButton = Tk.Button(tuningFrame, text = 'Show', command=lambda: controller.show_frame(lda,what=2), bg='blue', fg='white')
            featureButton.grid(row=3, column=4,sticky=E, padx=50)  
            
            
 
            rowN = 4
        else:
            rowN = 2
                   
        featuresFrame2 = Tk.LabelFrame(frame_results, text = "Evaluation", padx=10, pady=10)
        featuresFrame2.grid(row=rowN, columnspan = 6, sticky=W, padx=2)               
 
        
        rocimage = Tk.PhotoImage(file="images/placeholder.png")
        global ldaroclabel
        figureROC = ImageTk.PhotoImage(Image.open("images/placeholder.png"))
        ldaroclabel = Tk.Label(featuresFrame2)
        ldaroclabel.image = rocimage # keep a reference!
        ldaroclabel.grid(row=rowN+1, column=0,columnspan=2,pady=10,padx=10)
             
        global ldaprlabel
        ldaprlabel = Tk.Label(featuresFrame2)
        ldaprlabel.image = rocimage # keep a reference!
        ldaprlabel.grid(row=rowN+1,column=2,columnspan=2,pady=10,padx=10)
        
        global ldacmlabel
        ldacmlabel = Tk.Label(featuresFrame2)
        ldacmlabel.image = rocimage # keep a reference!
        ldacmlabel.grid(row=rowN+2,column=0,columnspan=2,pady=10,padx=10)
        
        global ldaperformancelabel
        ldaperformancelabel = Tk.Label(featuresFrame2)
        ldaperformancelabel.image = rocimage
        ldaperformancelabel.grid(row=rowN+2,column=2,columnspan=2,pady=10,padx=10)       

        rowN = rowN + 3
        
        featuresFrame = Tk.LabelFrame(frame_results, text = "Relevant Features", padx=10, pady=10)
        featuresFrame.grid(row=rowN, columnspan = 6, sticky=W, padx=2)

        featuresFrameQ = Tk.LabelFrame(featuresFrame, text = "", padx=10, pady=10, bg=BG_COLOR)
        featuresFrameQ.grid(row=rowN+1, columnspan = 6, sticky=W, padx=2) 
        
        featuresDes = Tk.Label(featuresFrameQ, text="Select number of features (taxa) most important in model", bg=BG_COLOR)
        featuresDes.grid(row=rowN+2, column=0,sticky=W,padx = 10,pady=1)
        
        ldafeaturesNVar = StringVar(featuresFrameQ, value="10")   
        featureEntry = Tk.Entry(featuresFrameQ, width=5,textvariable=ldafeaturesNVar)
        featureEntry.grid(row=rowN+2,column=1, sticky=W)
        featureTT = CreateToolTip(featureEntry,"Number of most contributing features")
   

        featureButton = Tk.Button(featuresFrameQ, text = 'Show', command=lambda: controller.show_frame(lda), bg='blue', fg='white')
        featureButton.grid(row=rowN+2, column=2,sticky=E, padx=50)         
        
        resultFrame = Frame(featuresFrame)  
        resultFrame.grid(row=rowN+3,columnspan=3,pady=30)  
        data = {'rec1': {'Feature': 'test', 'Score': 108.79, 'Class': 0},'rect2': {'Feature':'test2','Score': 108.79, 'Class': 1}} 

        ldaFeatureTable = TableCanvas(resultFrame, data = data,
                               editable=False,read_only=True,
                               reverseorder=1)
        ldaFeatureTable.show()  

        # Create a Tkinter variable
        global ldaexportTableVar
        ldaexportTableVar = StringVar(featuresFrame)

        # Dictionary with options
        choices = {"comma seperated - CSV", 
                   "tab seperated - TSV",
                   "excel file - XLSX" }
        ldaexportTableVar.set('comma seperated - CSV') # set the default option

        exportPopupMenu = Tk.OptionMenu(featuresFrame, ldaexportTableVar, *choices)
        exportPopupMenu.grid(row = rowN+4, column = 0,padx = 2, pady=1,sticky=E)

        exportButton = Tk.Button(featuresFrame, text = 'Export table', command=browse_export_feature, fg = 'white', bg = 'purple')
        exportButton.grid(row=rowN+4,column=1,sticky=W)
    def onFrameConfigure(self, event):
        canvas.configure(scrollregion=canvas.bbox("all"))

    def frameSize(self, event):
        canvas_width = event.width
        canvas_height = event.height
        canvas.itemconfig(frame_results, width = canvas_width, height = canvas_height)     

class logisticRegression(Tk.Frame):

    def __init__(self, parent, controller,tmpdir):
        print("logistic regressio")
        global lrLabelFig
        global lrFeatureTable
        global lrErrVar   
        global exportButton
        global lrfeaturesNVar
        global canvas
        global frame_results
        Tk.Frame.__init__(self,parent)


        canvas = Tk.Canvas(self,borderwidth=0, background=BG_COLOR, width=WIDTH, height=HEIGHT,scrollregion=(0,0,WIDTH+200,HEIGHT+500))
        #container.grid(row = 1, column = 0, sticky = "nsew")
        #container.pack(side="top", fill="both", expand=True)
        canvas.grid_rowconfigure(0, weight=1)
        canvas.grid_columnconfigure(0, weight=1)
        
        scrollbar = Tk.Scrollbar(self, orient="vertical")
        scrollbar.pack(fill = Y, side = RIGHT, expand = FALSE)
        scrollbar.config(command=canvas.yview)
        
        scrollbarx = Tk.Scrollbar(self, orient="horizontal")
        scrollbarx.pack(fill = X, side = BOTTOM, expand = FALSE)
        scrollbarx.config(command=canvas.xview)    
        canvas.configure(xscrollcommand = scrollbarx.set,yscrollcommand=scrollbar.set)
        canvas.config(width=WIDTH, height=HEIGHT)

#        canvas_frame = self.canvas.create_window((0,0), window=self, anchor="nw", tags="frame")
        canvas.pack(side=LEFT, expand=True, fill = BOTH)

        frame_results = Tk.Frame(canvas, bg=BG_COLOR)
        canvas_frame = canvas.create_window((0, 0), window=frame_results, anchor='nw', tags="frame")

        frame_results.bind("<Configure>", self.onFrameConfigure)
        canvas.bind('<Configure>', self.frameSize)


        label = Tk.Label(frame_results, text="Logistic Regression", font=LARGE_FONT,bg=BG_COLOR)
        label.grid(row=0,pady=10,columnspan=6,padx=10)
                
        lrErrVar = StringVar("")        
        lrError = Tk.Label(frame_results,textvariable=lrErrVar,fg="red",bg=BG_COLOR)
        lrError.grid(row=1, column=0, columnspan=6, pady=2,padx=2)   

        if( advanced == 1):
            tuningFrame = Tk.LabelFrame(frame_results, text = "Tuning", padx=10, pady=10)
            tuningFrame.grid(row=2, columnspan = 6, sticky=W, padx=2)

 
            tuningCDes = Tk.Label(tuningFrame, text="Enter the regularization parameter C")
            tuningCDes.grid(row=3, column=0,sticky=W,padx = 10,pady=1)
            
            global tuningCVar
            tuningCVar = StringVar(tuningFrame, value="1.0")   
            tuningCEntry = Tk.Entry(tuningFrame, width=5,textvariable=tuningCVar)
            tuningCEntry.grid(row=3,column=1, sticky=W)
            tuningCTT = CreateToolTip(tuningCEntry,"Must be a positive float. Smaller values specify stronger regularization.")
   
            tuningSolverDes = Tk.Label(tuningFrame, text="Select solver")
            tuningSolverDes.grid(row=3, column=2,sticky=W,padx = 10,pady=1)

            global tuningSolverVar
            solverVarChoices = {"newton-cg", "lbfgs", "liblinear", "sag", "saga"}
            tuningSolverVar = StringVar(tuningFrame, value="lbfgs")                       
            #tuningSolverVar.set('lbfgs') # set the default option
            tuningSolverPopup = Tk.OptionMenu(tuningFrame, tuningSolverVar, *solverVarChoices)
            tuningSolverPopup.grid(row=3,column=3, sticky=W)
            tuningCTT = CreateToolTip(tuningSolverPopup,"For small datasets, ‘liblinear’ is a good choice, whereas ‘sag’ and ‘saga’ are faster for large ones.")

            featureButton = Tk.Button(tuningFrame, text = 'Show', command=lambda: controller.show_frame(logisticRegression,what=2), bg='blue', fg='white')
            featureButton.grid(row=3, column=4,sticky=E, padx=50)  
            rowN = 4
        else:
            rowN = 2
                   
        featuresFrame2 = Tk.LabelFrame(frame_results, text = "Evaluation", padx=10, pady=10)
        featuresFrame2.grid(row=rowN, columnspan = 6, sticky=W, padx=2)               
 
    
        
        rocimage = Tk.PhotoImage(file="images/placeholder.png")
        global lrroclabel
        figureROC = ImageTk.PhotoImage(Image.open("images/placeholder.png"))
        lrroclabel = Tk.Label(featuresFrame2)
        lrroclabel.image = rocimage # keep a reference!
        lrroclabel.grid(row=rowN+1, column=0,columnspan=2,pady=10,padx=10)
             
        global lrprlabel
        lrprlabel = Tk.Label(featuresFrame2)
        lrprlabel.image = rocimage # keep a reference!
        lrprlabel.grid(row=rowN+1,column=2,columnspan=2,pady=10,padx=10)
        
        global lrcmlabel
        lrcmlabel = Tk.Label(featuresFrame2)
        lrcmlabel.image = rocimage # keep a reference!
        lrcmlabel.grid(row=rowN+2,column=0,columnspan=2,pady=10,padx=10)
        
        global lrperformancelabel
        lrperformancelabel = Tk.Label(featuresFrame2)
        lrperformancelabel.image = rocimage
        lrperformancelabel.grid(row=rowN+2,column=2,columnspan=2,pady=10,padx=10)    

        rowN = rowN + 3
        
        featuresFrame = Tk.LabelFrame(frame_results, text = "Relevant Features", padx=10, pady=10)
        featuresFrame.grid(row=rowN, columnspan = 6, sticky=W, padx=2)

        featuresFrameQ = Tk.LabelFrame(featuresFrame, text = "", padx=10, pady=10, bg=BG_COLOR)
        featuresFrameQ.grid(row=rowN+1, columnspan = 6, sticky=W, padx=2) 
        
        featuresDes = Tk.Label(featuresFrameQ, text="Select number of features (taxa) most important in model", bg=BG_COLOR)
        featuresDes.grid(row=rowN+2, column=0,sticky=W,padx = 10,pady=1)
        
        lrfeaturesNVar = StringVar(featuresFrameQ, value="10")   
        featureEntry = Tk.Entry(featuresFrameQ, width=5,textvariable=lrfeaturesNVar)
        featureEntry.grid(row=rowN+2,column=1, sticky=W)
        featureTT = CreateToolTip(featureEntry,"Number of most contributing features")
   

        featureButton = Tk.Button(featuresFrameQ, text = 'Show', command=lambda: controller.show_frame(logisticRegression), bg='blue', fg='white')
        featureButton.grid(row=rowN+2, column=2,sticky=E, padx=50)         
        
        resultFrame = Frame(featuresFrame)  
        resultFrame.grid(row=rowN+3,columnspan=3,pady=30)  
        data = {'rec1': {'Feature': 'test', 'Score': 108.79, 'Class': 0},'rect2': {'Feature':'test2','Score': 108.79, 'Class': 1}} 

        lrFeatureTable = TableCanvas(resultFrame, data = data,
                               editable=False,read_only=True,
                               reverseorder=1)
        lrFeatureTable.show()  

        # Create a Tkinter variable
        global lrexportTableVar
        lrexportTableVar = StringVar(featuresFrame)

        # Dictionary with options
        choices = {"comma seperated - CSV", 
                   "tab seperated - TSV",
                   "excel file - XLSX" }
        lrexportTableVar.set('comma seperated - CSV') # set the default option

        exportPopupMenu = Tk.OptionMenu(featuresFrame, lrexportTableVar, *choices)
        exportPopupMenu.grid(row = rowN+4, column = 0,padx = 2, pady=1,sticky=E)

        exportButton = Tk.Button(featuresFrame, text = 'Export table', command=browse_export_feature, fg = 'white', bg = 'purple')
        exportButton.grid(row=rowN+4,column=1,sticky=W)
    def onFrameConfigure(self, event):
        canvas.configure(scrollregion=canvas.bbox("all"))

    def frameSize(self, event):
        canvas_width = event.width
        canvas_height = event.height
        canvas.itemconfig(frame_results, width = canvas_width, height = canvas_height)         
        
class PageOne(Tk.Frame):

    def __init__(self, parent, controller,tmpdir):
        Tk.Frame.__init__(self, parent)
        
        labelTitle = Tk.Label(self, text="Step 1: Load metadata", font=LARGE_FONT,bg=BG_COLOR)
        labelTitle.grid(row=0, column=0, columnspan=3, pady=2)
        
        global labelInfo
        labelInfo = Tk.Label(self, text="",bg=BG_COLOR)
        labelInfo.grid(row=1, column=0, columnspan=3, pady=2)   
        
        buttonCounts = Tk.Button(self, text = 'Browse File', command=browse_afile, bg='gray', fg='white')
        buttonCounts.grid(row=2, column=0,sticky=W,padx = 10, pady=2)      
        
        labelDes = Tk.Label(self, text="Select Format",bg=BG_COLOR)
        labelDes.grid(row=3, column=0,sticky=W,padx = 10, pady=2)
    
        # Create a Tkinter variable
        global formatSelection
        formatSelection = StringVar(self)

        # Dictionary with options
        choices = {"comma seperated - CSV", 
                   "tab seperated - TSV",
                   "excel file - XLSX" }
        formatSelection.set('comma seperated - CSV') # set the default option

        formatPopupMenu = Tk.OptionMenu(self, formatSelection, *choices)
        formatPopupMenu.grid(row = 3, column = 1,padx = 10, pady=2)

        buttonCounts2 = Tk.Button(self, text = 'Upload', command=upload_afile, bg='gray', fg='white')
        buttonCounts2.grid(row=3, column=2,padx = 10, pady=2)    
        
  
 
        button2 = Tk.Button(self, text="Cancel") #todo quit application
        button2.grid(row=5, column=1,padx = 10, pady=15)       
        
        button2 = Tk.Button(self, text="Next Step",
                           command=lambda: controller.show_frame(PageTwo))
        button2.grid(row=5, column=2,padx = 10, pady=15)
        button2TT = CreateToolTip(button2, "Go to step 2: select annotation file")


def update_step2list():
    global mdata
    global Lb1
    
    mylist = mdata.columns.to_list()
    # Update the listbox
    # 1. clear all
    Lb1.delete(0, 'end')
    # 2. insert new data
    for met in mylist:
        Lb1.insert('end', met)
            
            
    
class PageTwo(Tk.Frame):

    def __init__(self, parent, controller,tmpdir):
        Tk.Frame.__init__(self, parent)

        labelTitle2 = Tk.Label(self, text="Select target variable", font=LARGE_FONT,bg=BG_COLOR)
        labelTitle2.grid(row=0, column=0, columnspan=3, pady=2)
        
        global labelInfo2Text
        labelInfo2Text = StringVar()
        labelInfo2 = Tk.Label(self, textvariable=labelInfo2Text,fg="red",bg=BG_COLOR)
        labelInfo2.grid(row=1, column=0, columnspan=3, pady=2)        
           
        labelDes= Tk.Label(self, text="Please select the target variable. This will be used as an outcome in the analysis.",bg=BG_COLOR)
        labelDes.grid(row=2, column=0, columnspan=3, pady=2) 
        global a
        global b
        global name
        global Location
        global X
        global Y
        global mdata
        mylist = mdata.columns.to_list()
       
        def on_click_listbox(name):
            global Location
            global mdata
            global labelInfo2
            widget = name.widget
            #get selected line index
            index = widget.curselection()
            #get the line's text
            name = widget.get(index[0])
            data1 = mdata[mdata[name] == 1]
            data2 = mdata[mdata[name] == 0]
            a = data1.shape[0]
            b = data2.shape[0]
            minimum = min(a,b)
            mdata = mdata.sample(n=len(mdata), random_state=42)
            Location = mdata.columns.get_loc(name)
            labelInfo2Text.set(name+" selected as target variable.")
            return(Location)
        frame = Frame(self)
        frame.grid(row=3,column=0,columnspan=3)    
        
        scrollbar = Scrollbar(frame, orient="vertical")
        scrollbar.pack(side="right",fill="y")
        global Lb1
        Lb1 = Listbox(frame)
        Lb1.pack(side="left", fill=Tk.BOTH,expand=True)
        Lb1.config(yscrollcommand=scrollbar.set)
        scrollbar.config(command=Lb1.yview)
        
        for met in mylist:
            Lb1.insert('end', met)
            
        Location = Lb1.bind('<<ListboxSelect>>', on_click_listbox)   
            
        button1 = Tk.Button(self, text="Previous Step",
                                command=lambda: controller.show_frame(PageOne))
        button1.grid(row=5, column=0,padx = 10, pady=15)      
        button1TT = CreateToolTip(button1, "Go to step 1: Load counts file")    

        button2 = Tk.Button(self, text="Cancel") #todo quit application
        button2.grid(row=5, column=1,padx = 10, pady=15)       
            
        button3 = Tk.Button(self, text="Next Step",
                               command=lambda: controller.show_frame(PageThree))
        button3.grid(row=5, column=2,padx = 10, pady=15)
        button3TT = CreateToolTip(button3, "Go to step 3: Load counts file")    
    
            
        
class PageThree(Tk.Frame):

    def __init__(self, parent, controller,tmpdir):
        Tk.Frame.__init__(self, parent)

        labelTitle3 = Tk.Label(self, text="Step 3: Load counts file", font=LARGE_FONT,bg=BG_COLOR)
        labelTitle3.grid(row=0, column=0, columnspan=3, pady=2)
        
        global labelInfo3Text
        labelInfo3Text = StringVar()
        labelInfo3 = Tk.Label(self, textvariable=labelInfo3Text,fg="red",bg=BG_COLOR)
        labelInfo3.grid(row=1, column=0, columnspan=3, pady=2)   
               
        buttonCounts = Tk.Button(self, text = 'Browse File', command=browse_cfile, bg='gray', fg='white')
        buttonCounts.grid(row=2, column=0,sticky=W,padx = 10, pady=2)      
        
        labelDes2 = Tk.Label(self, text="Select Format",bg=BG_COLOR)
        labelDes2.grid(row=3, column=0,sticky=W,padx = 10, pady=2)
    
        # Create a Tkinter variable
        global formatSelection2
        formatSelection2 = StringVar(self)

        # Dictionary with options
        choices2 = {"biom", 
                   "tab seperated"}
        formatSelection2.set('biom') # set the default option

        formatPopupMenu2 = OptionMenu(self, formatSelection2, *choices2)
        formatPopupMenu2.grid(row = 3, column = 1,padx = 10, pady=2)

        buttonCounts2 = Tk.Button(self, text = 'Upload', command=upload_cfile, bg='blue', fg='white')
        buttonCounts2.grid(row=3, column=2,padx = 10, pady=2)    
        
        button1 = Tk.Button(self, text="Previous Step",
                                command=lambda: controller.show_frame(PageOne))
        button1.grid(row=4, column=0,padx = 10, pady=15)      
        button1TT = CreateToolTip(button1, "Go to step 2: Select target variable")    

        button2 = Tk.Button(self, text="Cancel") #todo quit application
        button2.grid(row=4, column=1,padx = 10, pady=15)       
            
        button3 = Tk.Button(self, text="Next Step",
                               command=lambda: controller.show_frame(PageFour))
        button3.grid(row=4, column=2,padx = 10, pady=15)
        button3TT = CreateToolTip(button3, "Go to step 4: Normalization, Transformation")    
    
        
class PageFour(Tk.Frame):
    def __init__(self, parent, controller,tmpdir):            
        global minSampleDefault
        global tssChecked
        global rareTaxaDefault
        global transformSelection
        global diversitySelection
        
        Tk.Frame.__init__(self, parent)
        label = Tk.Label(self, text="Step 4: Normalization & Transformation & Adds", font=LARGE_FONT,bg=BG_COLOR)
        label.grid(row=0, column=0, columnspan=3, pady=2)
        
        labelTitle4 = Tk.Label(self, text="1. Data filter", font=LARGE_FONT,bg=BG_COLOR)
        labelTitle4.grid(row=1, column=0, columnspan=3, pady=2)

        minSampleLabel = Tk.Label(self, text="Minimum number of sequences in sample.",bg=BG_COLOR)
        minSampleLabel.grid(row=2,column=0,columnspan=2)

        
        minSampleDefault = StringVar(self, value="1000")   
        minSampleText = Tk.Entry(self, width=5,textvariable=minSampleDefault)
        minSampleText.grid(row=2,column=2, padx=10)
        minSampleTextTT = CreateToolTip(minSampleText,"Remove samples with less than minimum sequence reads. Use this filter only when uploading raw sequence counts. Set value to '0' to turn off filtering. Range: 0-1,000,000.")

        rareTaxaLabel = Tk.Label(self, text="Mimimun mean relative abundance [%]",bg=BG_COLOR) 
        rareTaxaLabel.grid(row=3,column=0,columnspan=2)
 
        rareTaxaDefault = StringVar(self, value="0.01")       
        rareTaxaText = Tk.Entry(self,width=5,textvariable=rareTaxaDefault)     
        rareTaxaText.grid(row=3,column=2)
        rareTaxaTextTT = CreateToolTip(rareTaxaText, text="Samples below threshold will be removed. Use this filter only when uploading raw sequence counts. Set value to '0' to turn off filtering. Range: 0-1,000,000.")

        labelTitle4 = Tk.Label(self, text="2. Data transformation", font=LARGE_FONT,bg=BG_COLOR)
        labelTitle4.grid(row=4, column=0, columnspan=3, pady=2)
        
        tssChecked = IntVar()
        tssCheck = Tk.Checkbutton(self, text="Total sum normalization (TSS)", variable=tssChecked,bg=BG_COLOR)
        tssCheck.grid(row=5, column=0,sticky=W)
        tssCheck.select()
        transformationLabel = Tk.Label(self, text="Data transformation",bg=BG_COLOR) 
        transformationLabel.grid(row=6,column=0,sticky=W,columnspan=2)
        
        transformSelection = StringVar(self)

        # Dictionary with options
        choices3 = {"log", 
                   "sqrt"}
        transformSelection.set('sqrt') # set the default option

        formatPopupMenu3 = OptionMenu(self, transformSelection, *choices3)
        formatPopupMenu3.grid(row = 6, column = 1,padx = 10, pady=2)       

        labelTitle5 = Tk.Label(self, text="3. Diversity indices", font=LARGE_FONT,bg=BG_COLOR)
        labelTitle5.grid(row=7, column=0, columnspan=3, pady=2)


        transformationLabel = Tk.Label(self, text="Calculate diversity indices",bg=BG_COLOR) 
        transformationLabel.grid(row=8,column=0,sticky=W,columnspan=2)
        
        diversitySelection = StringVar(self)
        choices4 = {"None", "All", "Shannon diversity", "Simpson diversity", "InvSimpson diversity","Richness"}
        diversitySelection.set('All') # set the default option
        formatPopupMenu4 = OptionMenu(self, diversitySelection, *choices4)
        formatPopupMenu4.grid(row = 8, column = 1,padx = 10, pady=2)   
        
        button1 = Tk.Button(self, text="Previous Step",
                            command=lambda: controller.show_frame(PageThree))
        button1.grid(row=9, column=0,padx = 10, pady=15)
        
        button2 = Tk.Button(self, text="Cancel",
                            command=lambda: controller.show_frame(PageThree))
        button2.grid(row=9, column=1,padx = 10, pady=15)

        button3 = Tk.Button(self, text="Next Step",
                            command=lambda: controller.show_frame(PageFive))
        button3.grid(row=9, column=2,padx = 10, pady=15)
        button3TT = CreateToolTip(button3, "Go to step 2: select annotation file")
        
class PageFive(Tk.Frame):
    def __init__(self, parent, controller,tmpdir):
        Tk.Frame.__init__(self, parent)
        label = Tk.Label(self, text="Predict", font=LARGE_FONT,bg=BG_COLOR)
        label.pack(pady=10,padx=10)

        button1 = Tk.Button(self, text="Previous Step",
                            command=lambda: controller.show_frame(PageFour))
        button1.pack()

        button2 = Tk.Button(self, text="Next Step",
                            command=lambda: controller.show_frame(Home))
        button2.pack()

print("starting alora")
import tempfile
global tmpdir
tmpdir = tempfile.mkdtemp(dir = tempfile.gettempdir(),suffix="alora")  

print(tmpdir)      
#Start the gui with the wizard
root = Wizard()
root.wm_title("Alora")
  
root.configure(bg=BG_COLOR)

 
root.mainloop()    

#root = Tk.Tk()
# root.wm_title("Alora")

# broButton = Tk.Button(master = root, text = 'Select the Excel sheet', width = 30, command=browse_file, bg='blue', fg='white')
# #broButton.pack(side=Tk.LEFT, padx = 2, pady=2)
# broButton.grid(row=2, column=1)
# broButton = Tk.Button(master = root, text = 'Ten most important Bacteria', width = 30, command=browse_file1, bg = 'white', fg = 'blue')
# #broButton.pack(side=Tk.RIGHT, padx = 5, pady=2)
# broButton.grid(row=4, column=1)
# broButton = Tk.Button(master = root, text = 'Save Results', width = 30, command=browse_file2, bg = 'white', fg = 'blue')
# #broButton.pack(side=Tk.RIGHT, padx = 10, pady=2)
# broButton.grid(row=8, column=1)
# broButton = Tk.Button(master = root, text = 'Algorithm Evaluation', width = 30, command=browse_file3, bg='blue', fg='white')
# #broButton.pack(side=Tk.RIGHT, padx = 10, pady=2)
# broButton.grid(row=6, column=1)
# path = "images/Alora - Data Analytics tool.jpg"

# #Creates a Tkinter-compatible photo image, which can be used everywhere Tkinter expects an image object.
# img = ImageTk.PhotoImage(Image.open(path))

# #The Label widget is a standard Tkinter widget used to display a text or image on the screen.
# panel = Tk.Label(root, image = img)
# panel.grid(column = 1, row = 1)
# #The Pack geometry manager packs widgets in rows or columns.
# #panel.pack(side = "bottom", fill = "both", expand = "yes")
# #root.geometry("600x300") #You want the size of the app to be 500x500
# root.resizable(0, 0) #Don't allow resizing in the x or y direction
# Tk.mainloop()

