#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Import scikit-learn dataset library
from sklearn import datasets
import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 
from sklearn.decomposition import PCA 
from sklearn.preprocessing import StandardScaler
import xgboost as xgb
from sklearn.metrics import mean_squared_error


# In[2]:


data = pd.read_excel(r'C:\Work\repository\Alora\example\ILEUM - MET.S.xlsx')
data = data.dropna()



# In[3]:


data.columns.get_loc("MET.S.BMI")


# In[4]:



X = data.iloc[:,1:400].values 
y = data.iloc[:,470].values


# In[5]:

features = list(data.iloc[:,1:400].columns) 
data_dmatrix = xgb.DMatrix(data=X,label=y, feature_names=features)


# In[6]:


from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)



# In[7]:




# In[8]:



xg_reg = xgb.XGBRegressor(objective ='reg:linear', colsample_bytree = 0.3, learning_rate = 0.1,
                max_depth = 5, alpha = 10, n_estimators = 10)

xg_reg.fit(X_train,y_train)
preds = xg_reg.predict(X_test)

print(type(xg_reg))





# In[9]:


rmse = np.sqrt(mean_squared_error(y_test, preds))
print("RMSE: %f" % (rmse))


# In[10]:


params = {"objective":"reg:linear",'colsample_bytree': 0.3,'learning_rate': 0.1,
                'max_depth': 5, 'alpha': 10}

cv_results = xgb.cv(dtrain=data_dmatrix, params=params, nfold=3,
                    num_boost_round=50,early_stopping_rounds=10,metrics="rmse", as_pandas=True, seed=123)


# In[11]:


cv_results.head()


# In[12]:


print((cv_results["test-rmse-mean"]).tail(1))


# In[13]:


xg_reg = xgb.train(params=params, dtrain=data_dmatrix, num_boost_round=10)


# In[14]:

#scores = xg_reg.feature_importances_
# intialise data of lists.
#featureScores = {'Feature':features,
 #       'Score':scores}
 
# Create DataFrame
#df = pd.DataFrame(featureScores)
#print(df.sort_values('Score', ascending=False))
from xgboost import plot_importance
xgb.plot_importance(xg_reg, max_num_features=10)

plt.show()


print(type(xg_reg))

# In[15]:


import graphviz 
import matplotlib.pyplot as plt

import os

os.environ["PATH"] += os.pathsep + 'C:\\Users\\marthaZ\\Anaconda3\\pkgs\\graphviz-2.38-hfd603c8_2\\Library\\bin\\graphviz'

xgb.plot_tree(xg_reg,num_trees=0)
# # plt.rcParams['figure.figsize'] = [50, 10]
# # plt.show()


# # In[ ]:


# xgb.plot_importance(xg_reg)
# plt.rcParams['figure.figsize'] = [5, 5]
# plt.show()

#booster = xg_reg.get_booster()
#original_feature_names = booster.feature_names
#print(booster.get_dump()[0])
